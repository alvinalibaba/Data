<?php
    include "koneksi.php"
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <form method="post">
      <nav class="navbar-dark bg-dark" style="height:55px;"></nav>
      <section style="background-image: ">
      <br style="margin-top: 150px;">
        <div class="container">
          <div class="row" style="background-color: rgba(255, 255, 255, 0.3);">
            <div class="col-md-12">
            <h2 align="center" style="color: black;margin-top: 5px; ">Data Karyawan</h2>
              <table class="table table-s triped table-dark text-center" style="margin-top:10px" align="center">
                <tr>
                  <td>ID</td>
                  <td>NIP</td>
                  <td>Tgl_Mulai_Lembur</td>
                  <td>Tgl_Akhir_Lembur</td>
                  <td>Masa Lembur</td>
                  <td>Akhir Lembur</td>
                </tr>

                <?php
                  $sql="SELECT * from tb_lembur";
                  $query=mysqli_query($con,$sql);
                  while ($data=mysqli_fetch_array($query)){
                ?>

                <tr>
                  <td><?php echo $data['id_lembur']; ?></td>
                  <td><?php echo $data['nip']; ?></td>
                  <td><?php echo $data['tgl_mulai_lembur']; ?></td>
                  <td><?php echo $data['tgl_akhir_lembur']; ?></td>
                  <td><?php echo $data['masa_waktu_lembur']; ?></td>
                  <td><?php echo $data['gaji_lembur']; ?></td>
                  <td>
                    <input type="submit" name="selesai" value="Selesai" class="btn btn-success">
                  </td>
                </tr>
                  <?php } ?>

              </div>
            </div>
          </div>
        </table>
      </section>
    </form>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>