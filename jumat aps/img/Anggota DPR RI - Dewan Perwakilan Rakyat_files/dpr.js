function getNewsContent(){
	$caption = $('#thumbs a.active img').attr('data-caption');
	$content = $('#thumbs a.active img').attr('data-content');
	$alias = $('#thumbs a.active img').attr('data-alias');
	$('.slider-news-active-caption a').html($caption);
	$('.slider-news-active-caption a').attr('href', $alias);
	$('.slider-news-active-content').html($content);
}
function getBeritaFotoContent(){
	$caption = $('#list-berita-foto .owl-item a.active img').attr('data-title');
	$content = $('#list-berita-foto .owl-item a.active img').attr('data-content');
	$('.small-title').html($caption);
	$('.small-content').html($content);
	$date = $('#list-berita-foto .owl-item a.active img').attr('data-date');
	if ( ! $date) $date = '';
	$('ul.foto-info').html('<li>' + $date + '</li>');
}

function openMenu(){
	$('#fixed-menu').animate({
		left:'0',
		opacity:'1'
	});
}
function openMenuResp(){
	$('#fixed-menu-resp').animate({
		left:'0',
		opacity:'1'
	});
}
function jsPane(){
	$('.jspane').each(function() {
		$(this).jScrollPane({ autoReinitialise: true, contentWidth: '0px' });
	});
}
$(function(){
	$('.tb-language').click(function(){
		$('.tb-language').removeClass('active');
		$(this).addClass('active');
	});
	$('#bw-slider-content.owl-carousel').owlCarousel({
		items: 1,
		loop: true,
		dots: true,
		autoplay: true,
		autoplayHoverPause: true
	});
	var owlNews = $('#slider-news.owl-carousel').owlCarousel({
		items: 1,
		loop: true,
		autoplay: false,
		autoplayHoverPause: true
	});
	owlNews.on('changed.owl.carousel', function(event) {
		a = event.item.index;
		c = event.item.count;
		i = a - 2;
		if (i == c) {
		 i = 0;
		}
		$('#thumbs a').removeClass('active');
		$('#thumbs a:eq('+i+')').addClass('active');
		owlNews.trigger('to.owl.carousel', [i, 250, true]);
		getNewsContent();
	});
	getNewsContent();
	$('#thumbs a').click(function() {
		$('#thumbs a').removeClass('active');
		$(this).addClass('active');
		i = $('#thumbs a').index(this);
		owlNews.trigger('to.owl.carousel', [i, 250, true]);
		getNewsContent();
	});
	var owlBeritaFoto = $('#berita-foto.owl-carousel').owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		navText: ['', ''],
		autoplay: false
	});
	owlBeritaFoto.on('changed.owl.carousel', function(event) {
		a = event.item.index;
		c = event.item.count;
		i = a - 2;
		if (i == c) {
		 i = 0;
		}
		$('#list-berita-foto .owl-item a').removeClass('active');
		$('#list-berita-foto .owl-item a:eq('+i+')').addClass('active');
		getBeritaFotoContent();
	});
	$('#list-berita-foto.owl-carousel').owlCarousel({
		items: 6,
		margin: 20,
		loop: false,
		nav: true,
		navText: ['', ''],
		autoplay: false,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 2
			},
			768: {
				items: 4
			},
			992: {
				items: 6
			}
		}
	});
	$('#list-berita-foto .owl-item a').click(function(){
		$('#list-berita-foto .owl-item a').removeClass('active');
		$(this).addClass('active');
		i = $('#list-berita-foto .owl-item a').index(this);
		owlBeritaFoto.trigger('to.owl.carousel', [i, 250, true]);
		getBeritaFotoContent();
	});
	$('#html5-close').html('<div class="html5-custom-close"></div>');
	$('.agenda-list').click(function(){
		$show = $(this).attr('data-show');
		$('.agenda-list').removeClass('active');
		$('.agenda-content-tab').removeClass('active');
		$(this).addClass('active');
		$('.agenda-content-' + $show).addClass('active');
	});
	$('#gallery.owl-carousel').owlCarousel({
		items: 4,
		margin: 20,
		loop: true,
		nav: true,
		navText: ['', ''],
		dots: true,
		autoplay: true,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 2
			},
			768: {
				items: 3
			},
			992: {
				items: 4
			}
		}
	});
	$('#fixed-menu .fm-item').hover(function(){
		if($(this).hasClass('hassubmenu')){
			$('.fm-item').removeClass('open');
			$(this).addClass('open');
		} else {
			$('.fm-item').removeClass('open');
		}
	});
	$('#fixed-menu-resp .fm-item').click(function(){
		if($(this).hasClass('hassubmenu')){
			if($(this).hasClass('open')){
				$(this).removeClass('open');
			} else {
				$(this).addClass('open');
			}
		} else {
			$('.fm-item').removeClass('open');
		}
	});
	$('#fixed-menu .close').click(function(){
		$('#fixed-menu').animate({
			left:'-325',
			opacity:'0'
		});
		$('.hassubmenu').removeClass('open');
	});
	$('#fixed-menu-resp .close').click(function(){
		$('#fixed-menu-resp').animate({
			left:'-100%',
			opacity:'0'
		});
	});
	$('#twitter.owl-carousel').owlCarousel({
		items: 4,
		margin: 20,
		loop: true,
		dots: true,
		autoplay: true,
		autoplayHoverPause: true,
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 2
			},
			992: {
				items: 4
			}
		}
	});
	$(".various").fancybox({
		maxWidth	: 850,
		fitToView	: false,
		width		: '80%',
		height		: '80%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	$('#accordion').find('.accordion-toggle').click(function(){
		if($(this).hasClass('active')) {
			$(this).removeClass('active');
			$(this).next().slideUp('fast');
			return false;
		}
		$(this).addClass('active');
		$(this).next().slideToggle('fast');
		var tableno = $(this).attr('data-no');
		var table = $('.'+tableno).DataTable();
		table.destroy();
		$('.'+tableno).DataTable({
			"pagingType": "full_numbers",
			"iDisplayLength": 10
		});
    });
	$('.options .list, .tb-layanan-informasi-publik').click(function(){
		if($(this).hasClass('open')) {
			$(this).find('.list-type').fadeOut();
			$(this).removeClass('open');
			return true;
		}
		$('.list-type').hide();
		$('.list').removeClass('open');
		$(this).addClass('open');
		$(this).find('.list-type').fadeIn();
	});
	if($('.list-propinsi').height() > 0) {jsPane();}
	$('.tb-pencarian').click(function(){
		$('.top-bar').fadeOut(function(){
			$('.top-search').fadeIn();
		});
	});
	$('.tb-pencarian-resp').click(function(){
		$('.header-resp').hide();
		$('.search-resp').fadeIn();
	});
	$('html').click(function(e) {
		if ( ! $(e.target).is('#tsearch') && ! $(e.target).is('.tb-pencarian') && ! $(e.target).is('#tsearch input')) {
			$('#tsearch').stop(true, true).fadeOut(function(){
				$('.top-bar').show();
			});
		}
		if ( ! $(e.target).is('.tb-pencarian.tb-pencarian-resp') && ! $(e.target).is('.tb-pencarian.tb-pencarian-resp .icon') && ! $(e.target).is('.search-resp') && ! $(e.target).is('.search-resp input') && ! $(e.target).is('.search-resp button')) {
			$('.header-resp').show();
			$('.search-resp').hide();
		}
		if ( ! $(e.target).is('.options') && ! $(e.target).is('.options li.list a')) {
			$('.list').removeClass('open');
			$('.list-widget').hide();
		}
	});
	$('.datatables').DataTable({
		"pagingType": "full_numbers",
		"iDisplayLength": 10
	});
	$('#table05').DataTable({
		"pagingType": "full_numbers",
		"iDisplayLength": 10,
		"columnDefs": [
			{  targets: 1, orderable: false }
		]
	});
	$('.paginate_button.first').html('<img src="/css/images/icon-back-paging02.png" />');
	$('.paginate_button.previous').html('<img src="/css/images/icon-back-paging01.png" />');
	$('.paginate_button.next').html('<img src="/css/images/icon-next-paging01.png" />');
	$('.paginate_button.last').html('<img src="/css/images/icon-next-paging02.png" />');
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	$(window).scroll(function(){
		if ($(this).scrollTop() > 205) {
			$('.scrollToTop').fadeIn();
			$('.share').addClass('fixed');
			$contentHeight = $('#detail').height();
			if ($(this).scrollTop() > ($contentHeight - 375)) {
				$('.share').removeClass('fixed');
				$('.share').addClass('fixed-bottom');
				$('.share').css('bottom',-Math.abs($contentHeight));
			} else {
				$('.share').addClass('fixed');
				$('.share').removeClass('fixed-bottom');
			}
		} else {
			$('.scrollToTop').fadeOut();
			$('.share').removeClass('fixed');
		}
	});
	if ($('#sekjen-treeview').length) {
		$('#sekjen-treeview').treeview();
	}

	/* BLOG.JS */
	$('.btn-replies').click(function() {
		$(this).parent().find('.btn-hidden').slideToggle();
	});
	
	$('.replay').click(function() {
		$(this).parent().find('.replay-show').slideToggle();
	});
	
	$('.btn-showmore').click(function() {
		$(this).parent().find('.blog-hide').show();
		$('.btn-showmore').hide();
	});
});