<?php 
  $dash = new KONTROLER();
  $dis  = $dash->getCountRows("table_distributor");
  $mer  = $dash->getCountRows("table_merek");
  $bar  = $dash->selectCount("table_barang","kd_barang");

  if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  }
 ?>
<div class="row">
        <div class="col-md-6 col-lg-4">
          <div class="widget-small primary coloured-icon"><i class="material-icons" style="font-size: 4rem; margin: 5px;">people</i>
            <div class="info">
              <h4>Distributor</h4>
              <p><b><?php echo $dis ?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="widget-small info coloured-icon"><i class="material-icons" style="font-size: 4rem; margin: 5px;">branding_watermark</i>
            <div class="info">
              <h4>Merek</h4>
              <p><b><?php echo $mer ?></b></p>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4">
          <div class="widget-small warning coloured-icon"><i class="material-icons" style="font-size: 4rem; margin: 5px;">widgets</i>
            <div class="info">
              <h4>Barang</h4>
              <p><b><?php echo $bar['count']; ?></b></p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Monthly Sales</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="tile">
            <h3 class="tile-title">Support Requests</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="pieChartDemo"></canvas>
            </div>
          </div>
        </div>
      </div>