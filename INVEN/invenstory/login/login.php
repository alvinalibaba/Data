<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="../css/mains.css">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <?php
		include "../kontroler.php";
		session_start();
		$iv = new KONTROLER();
		if ($iv->sessionCheck() == "true") {
			if ($_SESSION['level'] == "Admin") {
				header("location:../PageAdmin.php");
			}else if($_SESSION['level'] == "Kasir"){
				header("location:../PageKasir.php");
			}else if($_SESSION['level'] == "Manager"){
				header("location:../PageManager.php");
			}
		}
		if (isset($_POST['Login'])) {
			$username = $_POST['username'];
			$password = $_POST['password'];
			if ($response = $iv->login($username,$password)) {
				if ($response['response'] == "positive") {
					$_SESSION['username'] = $_POST['username'];
					$_SESSION['level']    = $response['level'];
					if ($response['level'] == "Admin") {
						echo "<script>window.location.href='../PageAdmin.php'</script>";
					}else if($response['level'] == "Kasir"){
						echo "<script>window.location.href='../PageKasir.php'</script>";
					}else if($response['level'] == "Manager"){
						echo "<script>window.location.href='../PageManager.php'</script>";
					}
				}
			}
		}
	 ?>
    <style>
    	.form-control::focus{
    		background-color: #172029;
    	}
    	.logo-new{
    		margin-top: -80px;
    		margin-bottom: -10px;
    		font-size: 40px;
    	}
      .samping{

        background-size: 200%;
        background-image: url(../2.jpg);
      }
      h1{
        font-size: 50px;
      }
      .judul{
        margin: 0 auto;
        margin-left: 40%;
      }
    </style>
</head>
<body>
<div class="row">
  <div class="col-md-4">
    <section class="login-content">
      <div class="login-box">
        <form class="login-form" method="post">
          <br><br>
          <div class="form-group">
            <label class="control-label" for="username">USERNAME</label>
            <input class="form-control" type="text" name="username" autocomplete="off" autofocus id="username">
          </div>
          <div class="form-group">
            <label class="control-label" for="password">PASSWORD</label>
            <input class="form-control" type="password" name="password" id="password">
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input type="checkbox" id="Stay"><span class="label-text" for="Stay">Stay Signed in</span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-secondary btn-block" name="Login"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
      </div>
    </section>
  </div>
  <div class="col-md-8 samping">
    <div class="login-content">
      <h1 class="judul">Invenstory</h1>
      <h2 class="judul">Cerita Gudang mu</h2>
    </div>
  </div>
</div>
	  


	<script src="../js/jquery-3.2.1.min.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/main.js"></script>
  <script src="../js/plugins/pace.min.js"></script>
  <script type="text/javascript" src="../js/plugins/sweetalert.min.js"></script>
  <?php include "../alerts_response.php" ?>
</body>
</html>