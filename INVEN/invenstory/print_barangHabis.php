<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dashboard Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/mains.css">

    <script src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
  </head>
  <style>
    body{
        overflow-x: hidden;
    }
  </style>
  <body onload="window.print();">
    <?php
      include 'kontroler.php';

      $qb = new KONTROLER();
      $dataB = $qb->edit("detailbarang","stok_barang",0);
      if (isset($_GET['export'])) {
        $dateNow = date("Y-m-d");
        header("Content-type:application/vnd-ms-excel");
        header("Content-Disposition:attachment;filename='$dateNow'-DataBarangHabis.xls");
      }
     ?>
  <style>
    @media print{
      button{
        display: none !important;
      }
      header{
        display: none !important;
      }
      aside{
        display: none !important;
      }
    }
  </style>
  <br><br>
  <div class="col-sm-12">
      <h3>Barang Yang Habis</h3>
      <hr>
      <div class="table-responsive-sm" id="tab">
      <table class="table table-hover table-bordered sampleTable" border="1" cellpadding="3" id="printTable">
                <thead>
                  <tr>
                    <th>Kode barang</th>
                    <th>Nama barang</th>
                    <th>Merek</th>
                    <th>Distributor</th>
                    <th>Tanggal Masuk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach($dataB as $ds){ ?>
          <tr>
            <td><?= $ds['kd_barang'] ?></td>
            <td><?= $ds['nama_barang'] ?></td>
            <td><?= $ds['merek'] ?></td>
            <td><?= $ds['nama_distributor'] ?></td>
            <td><?= $ds['tanggal_masuk'] ?></td>
            <td><?= number_format($ds['harga_barang']) ?></td>
            <td><?= $ds['stok_barang'] ?></td>
                  <?php $no++; } ?>
                </tbody>
              </table>
    </div>
</div>