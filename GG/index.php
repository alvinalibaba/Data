<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="alvin.css">
	<link rel="stylesheet" href="" ="bt/css/bootstrap.min.css">
	<link rel="stylesheet" href="animate.css">
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
	<link rel="stylesheet" href="fa/css/font-awesome.css">
</head>
<body>
	<div class="containner-fluid banner">
		<div class="row">
			<div class="col-md-12">
				<p class="inijudul">Hello!!</p>
				<div class="tombolnat">SCROLL GUYS</div>
			</div>
		</div>
	</div>

	<div class="container-fluid abouts">
		<div class="row">
			<div class="col-md-12">
				<div class="dskp text-center">About Me</div>
			</div>
		</div>		
	</div>

	<div class="container-fluid conbouts">
		<div class="row">
			<div class="col-md-12">
				<p class="contex">Halo nama saya Alvin ,ini adalah cv saya </p>
			</div>
		</div>

		<div class="row text-center">
			<div class="col-md-4">
				<div class="boxnat">
					<i class="fa fa-graduation-cap"></i>
					<p>SD</p>
					<p>2007-2013</p>
					<p class="namesekolah">SD ANANDA BOGOR</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="boxnat">
					<i class="fa fa-graduation-cap"></i>
					<p>SMP</p>
					<p>2013-2016</p>
					<p class="namesekolah">SMP MARDI YUANA BOGOR</p>
				</div>
			</div>
			<div class="col-md-4">
				<div class="boxnat">
					<i class="fa fa-graduation-cap"></i>
					<p>SMA</p>
					<p>2016-2019</p>
					<p class="namesekolah">SMK WIKRAMA BOGOR</p>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid abouts">
		<div class="row">
			<div class="col-md-12">
				<div class="dskp text-center">Skills</div>
			</div>
		</div>
	</div>

	<div class="container-fluid skl">
		<div class="row">
			<div class="col-md-6">
			<h1 class="text-center">Office</h1>
				<div class="boxnya">
					<div class="ids">WORD</div>
					<div class="skled">
						<div class="inskill s1"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">EXCEL</div>
					<div class="skled">
						<div class="inskill s2"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">POWER POINT</div>
					<div class="skled">
						<div class="inskill s3"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">VISIO</div>
					<div class="skled">
						<div class="inskill s4"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">SUBLIME</div>
					<div class="skled">
						<div class="inskill s5"></div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<h1 class="text-center">Programming</h1>
				<div class="boxnya">
					<div class="ids">BOOTSTRAP</div>
					<div class="skled">
						<div class="inskill s6"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">HTML</div>
					<div class="skled">
						<div class="inskill s7"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">CSS</div>
					<div class="skled">
						<div class="inskill s8"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">C++</div>
					<div class="skled">
						<div class="inskill s9"></div>
					</div>
				</div>
				<div class="boxnya">
					<div class="ids">VB.NET</div>
					<div class="skled">
						<div class="inskill s10"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row huw">
			<div class="col-md-12">
				<p class="txtbawah">Contact</p>

			</div>
		</div>
		<div class="row cont">
			<div class="col-md-6 pds">
				<input type="text" class="ues" placeholder="Your Name">
				<input type="email" class="ues" placeholder="Your Email">
				<textarea name="" id="" cols="30" rows="10" class="asd" placeholder="comment"></textarea>
				<div class="tombolnat">Send</div>

			</div>
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.385210597531!2d106.81288711442238!3d-6.598956466342432!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c5dbee0e7dc1%3A0x7a1042329171135!2sCiheuleut!5e0!3m2!1sid!2sid!4v1502717460522"></iframe>
			</div>
		</div>		
	</div>

	<footer>&copy; Copyright Alvin</footer>

</body>
</html>