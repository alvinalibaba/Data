<?php 
	$pg = new KONTROLER();
	$table = "table_user";
	$autokode = $pg->autokode($table,"kd_user","US");
	$pegawai  = $pg->querySelect("SELECT * FROM table_user WHERE level = 'Admin' OR level = 'Kasir'");
	$admin    = $pg->querySelect("SELECT * FROM table_user WHERE level = 'Admin'");
	$kasir    = $pg->querySelect("SELECT * FROM table_user WHERE level = 'Kasir'");
 	
 	if (isset($_POST['tmbh_pegawai'])) {
 		echo "<script>Test</script>";
			$kd_user  = $_POST['kode_user'];
			$name     = $_POST['nama_user'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			$confirm  = $_POST['confirm'];
			$level    = $_POST['level'];
			$redirect = "?page=kelAdmin";
			$response = $pg->register($kd_user,$name,$username,$password,$confirm,$level,$redirect);
 	}

 	if (isset($_GET['delete'])) {
 		$id = $_GET['id'];
 		$response = $pg->delete("table_user","kd_user",$id,"?page=kelAdmin");
 	}
 ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-4">
			<div class="tile">
				<h3>Form Pegawai</h3>
				<hr>
				<form method="post">
					<div class="form-group">
						<label for="">Kode Pegawai</label>
						<input type="text" class="form-control form-control-sm" name="kode_user" value="<?php echo $autokode; ?>" readonly>
					</div>
					<div class="form-group">
						<label for="">Nama Pegawai</label>
						<input type="text" class="form-control form-control-sm" name="nama_user" autofocus="on">
					</div>
					<div class="form-group">
						<label for="">Username</label>
						<input type="text" class="form-control form-control-sm" name="username">
					</div>
					<div class="form-group">
						<label for="">Password</label>
						<input type="password" class="form-control form-control-sm" name="password">
					</div>
					<div class="form-group">
						<label for="">Confirm Password</label>
						<input type="password" class="form-control form-control-sm" name="confirm">
					</div>
					<div class="form-group">
						<select name="level" class="form-control">
							<option value="Admin">Admin</option>
							<option value="Kasir">Kasir</option>
						</select>
					</div>
					<button class="btn btn-primary" name="tmbh_pegawai"><i class="fa fa-user-plus" ></i> Tambahkan Pegawai</button>
					<button type="reset" class="btn btn-danger"><i class="material-icons" style="font-size: 16px; font-weight: bold;">close</i> Cancel</button>
				</form>
			</div>
		</div>
		<div class="col-md-6">
		          <div class="tab-content">
		            <div class="tab-pane active" id="user-timeline">
		              <div class="tile user-settings">
		              	<h4 class="line-head">Semua Pegawai</h4>
		              	<div class="table-responsive-sm">
		              	<table class="table table-hover table-bordered" id="sampleTable">
						<thead>
							<tr>
							<th>Kode Pegawai</th>
							<th>Nama</th>
							<th>Username</th>
							<th>Level</th>
							<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$no =1;
							foreach($pegawai as $pgs){ ?>
							<tr>
								<td><?php echo $pgs['kd_user'] ?></td>
								<td><?php echo $pgs['nama_user'] ?></td>
								<td><?php echo $pgs['username'] ?></td>
								<td><?php echo $pgs['level'] ?></td>
								<td>
									<a href="#" class="btn btn-danger" id="btdelete<?php echo $no; ?>">Kick</a>
								</td>
							</tr>
							<script>
								$(document).ready(function(){
									$("#btdelete<?php echo $no; ?>").click(function(e){
										e.preventDefault();
										swal({
											title: "Hapus",
								            text: "Yakin Hapus?",
								            type: "warning",
								            showCancelButton: true,
								            confirmButtonText: "Yes",
								            cancelButtonText: "Cancel",
								      		closeOnConfirm: false,
								      		closeOnCancel: true
										}, function(isConfirm) {
							            if (isConfirm) {
							            	window.location.href="?page=kelAdmin&delete&id=<?php echo $pgs['kd_user'] ?>";
							            }
							          });
									});
								});
							</script>
							<?php $no++; } ?>
						</tbody>
					</table>
				</div>
		              </div>
		            </div>
		            <div class="tab-pane fade" id="user-settings">
		              <div class="tile user-settings">
		                <h4 class="line-head">Admin</h4>
		                <div class="table-responsive-sm">
		                <table class="table table-hover table-bordered" id="disTable">
							<thead>
								<tr>
								<th>Kode Pegawai</th>
								<th>Nama</th>
								<th>Username</th>
								<th>Level</th>
								<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if (count($admin) > 0) { 
									$no = 1;
								foreach($admin as $ad){ 
								?>
								<tr>
									<td><?php echo $ad['kd_user'] ?></td>
									<td><?php echo $ad['nama_user'] ?></td>
									<td><?php echo $ad['username'] ?></td>
									<td><?php echo $ad['level'] ?></td>
									<td>
										<a href="#" class="btn btn-danger" id="btdelete<?php echo $no; ?>">Kick</a>
									</td>
								</tr>
								<script>
									$(document).ready(function(){
										$("#btdelete<?php echo $no; ?>").click(function(e){
											e.preventDefault();
											swal({
												title: "Hapus",
									            text: "Yakin Hapus?",
									            type: "warning",
									            showCancelButton: true,
									            confirmButtonText: "Yes",
									            cancelButtonText: "Cancel",
									      		closeOnConfirm: false,
									      		closeOnCancel: true
											}, function(isConfirm) {
								            if (isConfirm) {
								            	window.location.href="?page=kelAdmin&delete&id=<?php echo $ad['kd_user'] ?>";
								            }
								          });
										});
									});
								</script>
								<?php $no++; } ?>
								<?php } ?>
							</tbody>
						</table>
					</div>
		              </div>
		            </div>
		            <div class="tab-pane fade" id="user-kasir">
		              <div class="tile user-settings">
		                <h4 class="line-head">Kasir</h4>
		                <div class="table-responsive-sm">
		                <table class="table table-hover table-bordered" id="disTable2">
							<thead>
								<tr>
								<th>Kode Pegawai</th>
								<th>Nama</th>
								<th>Username</th>
								<th>Level</th>
								<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no = 1;
								foreach($kasir as $ka){ 
								?>
								<tr>
									<td><?php echo $ka['kd_user'] ?></td>
									<td><?php echo $ka['nama_user'] ?></td>
									<td><?php echo $ka['username'] ?></td>
									<td><?php echo $ka['level'] ?></td>
									<td>
										<a href="#" class="btn btn-danger" id="btdelete<?php echo $no; ?>">Kick</a>
									</td>
								</tr>
								<script>
									$(document).ready(function(){
										$("#btdelete<?php $no; ?>").click(function(e){
											e.preventDefault();
											swal({
												title: "Hapus",
									            text: "Yakin Hapus?",
									            type: "warning",
									            showCancelButton: true,
									            confirmButtonText: "Yes",
									            cancelButtonText: "Cancel",
									      		closeOnConfirm: false,
									      		closeOnCancel: true
											}, function(isConfirm) {
								            if (isConfirm) {
								            	window.location.href="?page=kelAdmin&delete&id=<?php echo $ka['kd_user'] ?>";
								            }
								          });
										});
									});
								</script>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
		              </div>
		            </div>
		          </div>
		        </div>
		<div class="col-md-2">
		    <div class="tile p-0">
		        <ul class="nav flex-column nav-tabs user-tabs">
		            <li class="nav-item"><a class="nav-link active" href="#user-timeline" data-toggle="tab">Semua</a></li>
		            <li class="nav-item"><a class="nav-link" href="#user-settings" data-toggle="tab">Admin</a></li>
		            <li class="nav-item"><a class="nav-link" href="#user-kasir" data-toggle="tab">Kasir</a></li>
		        </ul>
			</div>
		</div>
	</div>
</div>