<?php 
	include "kontroler.php";
	$qb = new KONTROLER();
	$dataB = $qb->edit("detailbarang","stok_barang",0);
		$dateNow = date("Y-m-d");
		header("Content-type:application/vnd-ms-excel");
		header("Content-Disposition:attachment;filename='$dateNow'-DataBarangHabis.xls");
 ?>
 	<style>
 		@media print{
 			button{
 				display: none !important;
 			}
 			header{
 				display: none !important;
 			}
 			aside{
 				display: none !important;
 			}
 		}
 	</style>
	<div class="col-sm-12" style="background: white; padding: 50px;">
		<!-- <div class="tile"> -->
			<h3>Barang Yang Habis</h3>
			<a href="export.php" name="export" class="btn btn-success" target="_blank" style="height: 75px; width: 105px;"><i class="material-icons" style="font-size: 30px;">content_paste</i><center>Export Excel</center></a>
        	<button class="btn btn-info" style="height: 75px; width: 75px;" id="print"><i class="material-icons" style="font-size: 30px;">local_printshop</i><center>Print</center></button>
			<hr>
			<br>
			<br>
			<div class="table-responsive-sm">
			<table border="1" cellspacing="0" width="100%;" cellpadding="20" id="printTable">
                <thead>
                  <tr>
                    <th>Kode barang</th>
                    <th>Nama barang</th>
                    <th>Merek</th>
                    <th>Distributor</th>
                    <th>Tanggal Masuk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($dataB as $ds){ ?>
					<tr>
						<td><?= $ds['nama_barang'] ?></td>
						<td><?= $ds['merek'] ?></td>
						<td><?= $ds['nama_distributor'] ?></td>
						<td><?= $ds['tanggal_masuk'] ?></td>
						<td><?= number_format($ds['harga_barang']) ?></td>
						<td><?= $ds['stok_barang'] ?></td>
                  <?php $no++; } ?>
                </tbody>
              </table>
          </div>
		<!-- </div> -->
	</div>
	<script type="text/javascript">
		function printData()
		{
		   var divToPrint=document.getElementById("printTable");
		   newWin= window.open("");
		   newWin.document.write(divToPrint.outerHTML);
		   newWin.print();
		   newWin.close();
		}

		$('#print').on('click',function(){
			printData();
		})
	</script>