<?php 
	$br = new KONTROLER();
	if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  	}
	$table = "table_barang";	
	$getMerek = $br->select("table_merek");
	$getDistr = $br->select("table_distributor");
	$autokode = $br->autokode("table_barang","kd_barang","BR");
	$waktu    = date("Y-m-d");
	if (isset($_POST['getSimpan'])) {
		$kode_barang  = $br->validateHtml($_POST['kode_barang']);
		$nama_barang  = $br->validateHtml($_POST['nama_barang']);
		$merek_barang = $br->validateHtml($_POST['merek_barang']);
		$distributor  = $br->validateHtml($_POST['distributor']);
		$harga        = $br->validateHtml($_POST['harga']);
		$stok         = $br->validateHtml($_POST['stok']);
		$foto         = $_FILES['foto'];
		$ket   = $br->validateHtml($_POST['ket']);

		if ($kode_barang == " " || $nama_barang == " " || $merek_barang == " " || $distributor == " " || $harga == " " || $stok == " " || $foto == " " || $ket == " " ) {
			$response = ['response'=>'negative','alert'=>'lengkapi field'];
		}else{
			if ($harga < 0 || $stok < 0 || $harga == 0 || $stok == 0) {
			 	$response = ['response'=>'negative','alert'=>'harga atau stok tidak boleh kurang dari 1'];
			}else{
				$response = $br->validateImage();
				if (@$response['types'] == "true") {
					$value = "'$kode_barang','$nama_barang','$merek_barang','$distributor','$waktu','$harga','$stok','$response[image]','$ket'";

					$response = $br->insert($table,$value,"?page=view_barang");
				}
			} 
		}
	}
 ?>
<div class="row">
	<div class="col-sm-12">
		<div class="tile">
			<h3>Input Barang</h3>
			<hr>
			<form method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="kode_barang">Kode barang</label>
						<input type="text" class="form-control" name="kode_barang" value="<?php echo $autokode; ?>" readonly>
					</div>
					<div class="form-group">
						<label for="nama_barang">Nama barang</label>
						<input type="text" class="form-control" name="nama_barang" value="<?php echo @$_POST['nama_barang'] ?>">
					</div>
					<div class="form-group">
						<label for="merek_barang">Merek</label>
						<select name="merek_barang" class="form-control">
							<option value=" ">Pilih merek</option>
							<?php foreach($getMerek as $mr) { ?>
							<option value="<?= $mr['kd_merek'] ?>"><?= $mr['merek'] ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label for="distributor">Distributor</label>
						<select name="distributor" class="form-control">
							<option value=" ">Pilih distributor</option>
							<?php foreach($getDistr as $dr) { ?>
							<option value="<?= $dr['kd_distributor'] ?>"><?= $dr['nama_distributor'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group">
						<label for="harga">Harga barang</label>
						<input type="number" class="form-control" name="harga">
					</div>
					<div class="form-group">
						<label for="stok">Stok barang</label>
						<input type="number" class="form-control" name="stok">
					</div>
					<div class="form-group">
						<label for="">Foto</label>
						<input type="file" class="form-control" name="foto">
					</div>
					<div class="form-group">
						<label for="ket">Keterangan</label>
						<input type="text" class="form-control" name="ket">
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-sm-12">
					<button style="width: 75px; height: 75px; margin-left: 85%;" type="submit" name="getSimpan" class="btn btn-primary"><i class="material-icons">save</i><center> Simpan</center></button>
					<button style="width: 75px; height: 75px;" type="reset" class="btn btn-danger"><i class="material-icons">close</i><center> Batal</center></button>
					<!-- <button type="reset" class="btn btn-danger"><i class="fa fa-repeat"></i>Reset</button> -->
				</div>
			</div>
		</div>
		</form>
	</div>
</div>