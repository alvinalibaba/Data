
<?php 
	$qb = new KONTROLER();
	$dataB = $qb->select("detailbarang");
	if ($_SESSION['level'] != "Manager") {
    header("location:login.php");
  	}
 ?>
<div class="row">
	<div class="col-sm-12">
		<div class="tile">
      <div class="container-fluid">
      <h3>Semua Barang</h3>
      <hr>
        <a href="export.php" name="export" class="btn btn-success" target="_blank" style="height: 75px; width: 105px;"><i class="material-icons" style="font-size: 30px;">content_paste</i><center>Export Excel</center></a>
        <a href="databarangfull.php" target="_blank" class="btn btn-info" style="height: 75px; width: 75px;"><i class="material-icons" style="font-size: 30px;">local_printshop</i><center>Print</center></a>
      <hr>
      <div class="table-responsive-sm">
			<table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>Kode barang</th>
                    <th>Nama barang</th>
                    <th>Merek</th>
                    <th>Distributor</th>
                    <th>Tanggal Masuk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($dataB as $ds){ ?>
					<tr>
						<td><?= $ds['kd_barang'] ?></td>
						<td><?= $ds['nama_barang'] ?></td>
						<td><?= $ds['merek'] ?></td>
						<td><?= $ds['nama_distributor'] ?></td>
						<td><?= $ds['tanggal_masuk'] ?></td>
						<td><?= number_format($ds['harga_barang']) ?></td>
						<td><?= $ds['stok_barang'] ?></td>
						<td class="text-center">
								<a href="?page=view_detailM&id=<?php echo $ds['kd_barang'] ?>" class="btn btn-warning"><i class="fa fa-search"></i></a>
						</td>
					</tr>
                  <?php $no++; } ?>
                </tbody>
              </table>
      </div>
      </div>
		</div>
	</div>
</div>