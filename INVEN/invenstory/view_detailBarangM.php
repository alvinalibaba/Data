<?php 
	$dt = new KONTROLER();
	$detail = $dt->selectWhere("detailbarang","kd_barang",$_GET['id']);
    if ($_SESSION['level'] != "Manager") {
    header("location:login.php");
    }
 ?>
<div class="row">
	<div class="col-sm-4">
        <div class="bs-component">
            <div class="card">
                <div class="card-body">
                  <h5 class="card-title"><?php echo $detail['nama_barang']; ?></h5>
                  <h6 class="card-subtitle text-muted">Merek</h6>
                </div><img style="min-height: 200px; width: 100%; display: block;" src="img/<?php echo $detail['gambar'] ?>" alt="Card image">
            </div>
            <br>
            <a href="?page=kelBarang" class="btn btn-danger"><i class="fa fa-repeat"></i> Kembali</a>
        </div>
    </div>
    <div class="col-sm-8">
    	<div class="bs-componet">
    		<div class="card">
    			<div class="card-body">
    				<h3>Detail Barang</h3>
    				<table class="table" cellpadding="10">
    					<tr>
    						<td>Kode Barang</td>
    						<td>:</td>
    						<td><?php echo $detail['kd_barang']; ?></td>
    					</tr>
    					<tr>
    						<td>Nama Barang</td>
    						<td>:</td>
    						<td><?php echo $detail['nama_barang']; ?></td>
    					</tr>
    					<tr>
    						<td>Merek</td>
    						<td>:</td>
    						<td><?php echo $detail['merek']; ?></td>
    					</tr>
    					<tr>
    						<td>Distributor</td>
    						<td>:</td>
    						<td><?php echo $detail['nama_distributor']; ?></td>
    					</tr>
    					<tr>
    						<td>Tanggal Masuk</td>
    						<td>:</td>
    						<td><?php echo $detail['tanggal_masuk']; ?></td>
    					</tr>
    					<tr>
    						<td>Harga</td>
    						<td>:</td>
    						<td><?php echo "Rp.".number_format($detail['harga_barang'])."-,"; ?></td>
    					</tr>
    					<tr>
    						<td>Stok</td>
    						<td>:</td>
    						<td><?php echo $detail['stok_barang'] ?></td>
    					</tr>
    					<tr>
    						<td>Keterangan</td>
    						<td>:</td>
    						<td><?php echo $detail['keterangan'] ?></td>
    					</tr>
    				</table>
    			</div>
    		</div>
    	</div>
    </div>
</div>