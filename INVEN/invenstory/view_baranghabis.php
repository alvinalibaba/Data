<?php
	$qb = new KONTROLER();
	$dataB = $qb->edit("detailbarang","stok_barang",0);
	if (isset($_GET['export'])) {
		$dateNow = date("Y-m-d");
		header("Content-type:application/vnd-ms-excel");
		header("Content-Disposition:attachment;filename='$dateNow'-DataBarangHabis.xls");
	}
 ?>
 	<style>
 		@media print{
 			button{
 				display: none !important;
 			}
 			header{
 				display: none !important;
 			}
 			aside{
 				display: none !important;
 			}
 		}
 	</style>
	<div class="col-sm-12">
		<div class="tile">
			<h3>Barang Yang Habis</h3>
			<a href="export_barangHabis.php" name="export" class="btn btn-success" target="_blank" style="height: 75px; width: 105px;"><i class="material-icons" style="font-size: 30px;">content_paste</i><center>Export Excel</center></a>
        	<a href="print_barangHabis.php" id="btnPrint" target="_blank" class="btn btn-info" style="height: 75px; width: 75px;"><i class="material-icons" style="font-size: 30px;">local_printshop</i><center>Print</center></a>

			<hr>
			<div class="table-responsive-sm" id="tab">
			<table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>Kode barang</th>
                    <th>Nama barang</th>
                    <th>Merek</th>
                    <th>Distributor</th>
                    <th>Tanggal Masuk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach($dataB as $ds){ ?>
					<tr>
						<td><?= $ds['kd_barang'] ?></td>
						<td><?= $ds['nama_barang'] ?></td>
						<td><?= $ds['merek'] ?></td>
						<td><?= $ds['nama_distributor'] ?></td>
						<td><?= $ds['tanggal_masuk'] ?></td>
						<td><?= number_format($ds['harga_barang']) ?></td>
						<td><?= $ds['stok_barang'] ?></td>
                  <?php $no++; } ?>
                </tbody>
              </table>
		</div>
	</div>
</div>