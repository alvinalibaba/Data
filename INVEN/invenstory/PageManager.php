<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dashboard Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/mains.css">
    <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/iconfont/material-icons.css">

    <script src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>

    <style type="text/css">
      @font-face {
        font-family: 'Orbitron';
        src: url('Orbitron/Orbitron-Regular.ttf');
        src: url('Orbitron/Orbitron-Regular.ttf') format('ttf'),
             url('Orbitron/Orbitron-Regular.ttf') format('truetype');
      }
    </style>
  </head>

  <?php 
  include "kontroler.php";
  $iv = new KONTROLER();
  session_start();
  $Auth = $iv->AuthUser($_SESSION['username']);
  // session_destroy();
  if ($iv->sessionCheck() == "false") {
    header("location:login.php");
  }

  if ($_SESSION['level'] != "Manager") {
    header("location:login.php");
  }

  if (isset($_GET['logout'])) {
    $iv->logout();
  }
 ?>
  <body class="app sidebar-mini rtl">
    <header class="app-header"><a class="app-header__logo" href="#" style="font-size: 2em;">Sarpras</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item fa fa-user-circle fa-2x" href="#" data-toggle="dropdown" aria-label="Open Profile Menu" style="text-decoration: none;"></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="?page=profile"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="#" id="forLogout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        <div>
          <p class="app-sidebar__user-name"><?php echo $Auth['nama_user'] ?></p>
          <p class="app-sidebar__user-designation"><?php echo $_SESSION['level']; ?></p>
        </div>
      </div>
      <hr>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="?page"><i class="material-icons" style="margin-right: 5px;">dashboard</i><span class="app-menu__label"> Dashboard</span></a></li>
        <li><a class="app-menu__item" href="?page=kelAdmin"><i class="material-icons" style="margin-right: 5px;">group_add</i><span class="app-menu__label"> Kelola Pegawai</span></a></li>
        <li class="treeview"><a class="app-menu__item" href="#" data-toggle="treeview"><i class="material-icons" style="margin-right: 5px;">widgets</i><span class="app-menu__label"> Data Barang</span><i class="treeview-indicator fa fa-angle-right"></i></a>
          <ul class="treeview-menu">
            <li><a class="treeview-item" href="?page=kelBarang"><i class="icon fa fa-circle-o"></i>Semua Barang</a></li>
            <li><a class="treeview-item" href="?page=periode"><i class="icon fa fa-circle-o"></i>Lihat Barang Per Periode</a></li>
            <li><a class="treeview-item" href="?page=barangHabis"><i class="icon fa fa-circle-o"></i>Barang Habis</a></li>
          </ul>
        </li>
      </ul>
    </aside>
    <main class="app-content">
    <div class="row">
      <?php 
        @$page = $_GET['page'];
        switch ($page) {
          case 'kelAdmin':
            include "view_kelolaAdmin.php";
          break;
          case 'kelBarang':
            include "view_managerBarang.php";
          break;
          case 'view_detailM':
            include "view_detailBarangM.php";
          break;
          case 'periode':
            include "barangperiode.php";
          break;
          case 'barangHabis':
            include "view_baranghabis.php";
          break;
          case 'profile':
            include "profile.php";
          break;
          default:
            include "DashboardManager.php";
            break;
        }
       ?>
    </div>
    </main>


    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/plugins/chart.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="js/plugins/select2.min.js"></script>
    <script type="text/javascript" src="js/plugins/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $('#sampleTable').DataTable();
      $('.sampleTable').DataTable();
      $('#disTable').DataTable();
      $('#disTable2').DataTable();
    </script>
    <?php include "alerts_response.php"; ?>
    <?php include "alerts_response.php"; ?>
    <script>
      $(document).ready(function(){
        $('#forLogout').click(function(e){
          e.preventDefault();
            swal({
            title: "Logout",
            text: "Yakin Logout?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
          }, function(isConfirm) {
            if (isConfirm) {
              window.location.href="?logout";
            }
          });
        });
        $('#raditor').DataTable();
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){

      $('#demoDate').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true
      });

      });
    </script>
  </body>
</html>