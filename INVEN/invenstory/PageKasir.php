<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dashboard Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/mains.css">
    <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/iconfont/material-icons.css">  

    <script src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
  
    <style type="text/css">
      @font-face {
        font-family: 'Orbitron';
        src: url('Orbitron/Orbitron-Regular.ttf');
        src: url('Orbitron/Orbitron-Regular.ttf') format('ttf'),
             url('Orbitron/Orbitron-Regular.ttf') format('truetype');
      }
    </style>
  </head>
  <?php 
  include "kontroler.php";
  $iv = new KONTROLER();
  session_start();
  $Auth = $iv->AuthUser($_SESSION['username']);

  if ($iv->sessionCheck() == "false") {
    header("location:login.php");
  }

  if ($_SESSION['level'] != "Kasir") {
    header("location:login.php");
  }

  if (isset($_GET['logout'])) {
    $iv->logout();
  }
 ?>
  <body class="app sidebar-mini rtl">
    <header class="app-header"><a class="app-header__logo" href="#" style="font-size: 2em;">Sarpras</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item fa fa-user-circle fa-2x" href="#" data-toggle="dropdown" aria-label="Open Profile Menu" style="text-decoration: none;"></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="?page=profile"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="#" id="forLogout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        <div>
          <p class="app-sidebar__user-name"><?php echo $Auth['nama_user'] ?></p>
          <p class="app-sidebar__user-designation"><?php echo $_SESSION['level']; ?></p>
        </div>
      </div>
      <hr>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="?page"><i class="material-icons" style="margin-right: 5px;">dashboard</i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="?page=kasirTransaksi"><i class="material-icons" style="margin-right: 5px;">shopping_cart</i><span class="app-menu__label">Transaksi</span></a></li>
      </ul>
    </aside>
    <main class="app-content">

      <?php 
        @$page = $_GET['page'];
        switch ($page) {
          case 'profile':
            include "profile.php";
          break;
          case 'kasirTransaksi':
            include "kasir_transaksi.php";
          break;
          case 'pembayaran':
            include "kasir_pembayaran.php";
          break;
          case 'struk':
            include "struk_akhir.php";
          break;
          default:
            include "kasirDashboard.php";
            break;
        }
       ?>
    </main>


    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/plugins/chart.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
      $('#sampleTable').DataTable();
      $('#disTable').DataTable();
    </script>
    <?php include "alerts_response.php"; ?>
    <?php include "alerts_response.php"; ?>
    <script>
      $(document).ready(function(){
        $('#forLogout').click(function(e){
          e.preventDefault();
            swal({
            title: "Logout",
            text: "Yakin Logout?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
          }, function(isConfirm) {
            if (isConfirm) {
              window.location.href="?logout";
            }
          });
        });
        $('#raditor').DataTable();
      });
    </script>
    <script type="text/javascript">
      var data = {
        labels: ["Senin", "Selasa", "Rabu", "Kamis", "Jumat"],
        datasets: [
          {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [0, 0, 10, 21, 22]
          }
        ]
      };
      var pdata = [
        {
          value: 100,
          color: "#46BFBD",
          highlight: "#172029",
          label: "Complete"
        },
        {
          value: 200,
          color:"#F7464A",
          highlight: "#FF5A5E",
          label: "In-Progress"
        }
      ]
      
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);
      
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    </script>
  </body>
</html>