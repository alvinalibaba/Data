<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dashboard Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/mains.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
  </head>
  <style>
    body{
     	overflow-x: hidden;
    }
  </style>
  <body onload="window.print();">
<?php 
include "kontroler.php";
$qb = new KONTROLER();
$dataB = $qb->select("detailbarang");
$totbal = $qb->selectSum("detailbarang","stok_barang");
$total  = $qb->selectCount("detailbarang","kd_barang");
 ?>
 <div class="row">
 	<div class="col-sm-12" style="padding: 50px;">
      <h3>Data Barang Invenstory</h3>
      <p>Tanggal : <?php echo date("Y-m-d"); ?></p>
 	</div>
 </div>
<div class="row">
	<div class="col-sm-12">
			<table class="table table-hover table-bordered sampleTable" border="1" cellspacing="0" width="100%;" cellpadding="20">
                <thead>
                  <tr>
                    <th>Kode barang</th>
                    <th>Nama barang</th>
                    <th>Merek</th>
                    <th>Distributor</th>
                    <th>Tanggal Masuk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach($dataB as $ds){ ?>
					<tr>
						<td><?= $ds['kd_barang'] ?></td>
						<td><?= $ds['nama_barang'] ?></td>
						<td><?= $ds['merek'] ?></td>
						<td><?= $ds['nama_distributor'] ?></td>
						<td><?= $ds['tanggal_masuk'] ?></td>
						<td><?= number_format($ds['harga_barang']) ?></td>
						<td><?= $ds['stok_barang'] ?></td>
                  <?php $no++; } ?>
                </tbody>
                <tr>
                	<td colspan="6">Jumlah Total stok barang yg dimiliki</td>
                	<td><?php echo $totbal['sum']; ?></td>
                </tr>
                <tr>
                	<td colspan="6">Barang yg ada</td>
                	<td><?php echo $total['count']; ?></td>
                </tr>
              </table>
		</div>
</div>

</body>
</html>