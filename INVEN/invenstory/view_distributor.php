<?php 
	$dis = new KONTROLER();
	if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  	}
	$table = "table_distributor";
	$dataDis = $dis->select($table);
	$autokode = $dis->autokode($table,"kd_distributor","DS");

	if (isset($_GET['delete'])) {
		$where = "kd_distributor";
		$whereValues = $_GET['id'];
		$redirect = "?page=view_distributor";
		$response = $dis->delete($table,$where,$whereValues,$redirect);
	}

	if (isset($_GET['edit'])) {
		$id = $_GET['id'];
		$editData = $dis->selectWhere($table,"kd_distributor",$id);
		$autokode = $editData['kd_distributor'];
	}
	if (isset($_POST['getSave'])) {
		$kd_distributor   = $dis->validateHtml($_POST['kode_distributor']);
		$nama_distributor = $dis->validateHtml($_POST['nama_distributor']);
		$nohp_distributor = $dis->validateHtml($_POST['nohp_distributor']);
		$alamat           = $dis->validateHtml($_POST['alamat']);

		if ($kd_distributor == " " || empty($kd_distributor) || $nama_distributor == " " || empty($nama_distributor) || $nohp_distributor == " " || empty($nohp_distributor) || $alamat == " " || empty($alamat)) {
			$response = ['response'=>'negative','alert'=>'lengkapi field'];
		}else{
			$validno = substr($nohp_distributor, 0,2);
			if ($validno != "08") {
				$response = ['response'=>'negative','alert'=>'Masukan nohp yang valid'];
			}else{
				if (strlen($nohp_distributor) < 11) {
					$response = ['response'=>'negative','alert'=>'Masukan 11 digit nohp'];
				}else{
					$value = "'$kd_distributor','$nama_distributor','$alamat','$nohp_distributor'";
					$response = $dis->insert($table,$value,"?page=view_distributor");
				}
			}
		}
	}

	if (isset($_POST['getUpdate'])) {
		$kd_distributor   = $dis->validateHtml($_POST['kode_distributor']);
		$nama_distributor = $dis->validateHtml($_POST['nama_distributor']);
		$nohp_distributor = $dis->validateHtml($_POST['nohp_distributor']);
		$alamat           = $dis->validateHtml($_POST['alamat']);

		if ($kd_distributor == "" || $nama_distributor == "" || $nohp_distributor == "" || $alamat == "") {
			$response = ['response'=>'negative','alert'=>'lengkapi field'];
		}else{
			$validno = substr($nohp_distributor, 0,2);
			if ($validno != "08") {
				$response = ['response'=>'negative','alert'=>'Masukan nohp yang valid'];
			}else{
				if (strlen($nohp_distributor) < 11) {
					$response = ['response'=>'negative','alert'=>'Masukan 11 digit nohp'];
				}else{
					$value = "kd_distributor='$kd_distributor',nama_distributor='$nama_distributor',no_telp='$nohp_distributor',alamat='$alamat'";
					$response = $dis->update($table,$value,"kd_distributor",$_GET['id'],"?page=view_distributor");
				}
			}
		}
	}
 ?>
<div class="container-fluid">
<div class="row">
	<div class="col-sm-4">
		<div class="tile">
			<h3>Tambah Distributor</h3>
			<hr>
			<form method="post">
			<div class="form-group">
				<label for="">Kode distributor</label>
				<input type="text" class="form-control form-control-sm" name="kode_distributor" value="<?php echo $autokode; ?>" readonly>
			</div>
			<div class="form-group">
				<label for="">Nama distributor</label>
				<input type="text" class="form-control form-control-sm" name="nama_distributor" value="<?php echo @$editData['nama_distributor'] ?>">
			</div>
			<div class="form-group">
				<label for="">No handphone distributor</label>
				<input type="text" class="form-control form-control-sm" name="nohp_distributor" value="<?php echo @$editData['no_telp']; ?>">
			</div>
			<div class="form-group">
				<label for="">Alamat</label>
				<textarea name="alamat" rows="3" class="form-control"><?php echo @$editData['alamat'] ?></textarea>
			</div>
			<hr>
			<?php if (isset($_GET['edit'])): ?>
			<button type="submit" name="getUpdate" class="btn btn-warning"><i class="fa fa-check"></i> Update</button>
			<a href="?page=view_distributor" class="btn btn-danger">Cancel</a>
			<?php endif ?>
			<?php if (!isset($_GET['edit'])): ?>	
			<button type="submit" name="getSave" class="btn btn-primary"><i class="material-icons">save</i><center> Simpan</center></button>
			<?php endif ?>
		</div>
		</form>
	</div>
	<div class="col-sm-8">
		<div class="tile">
			<h3>Data Distributor</h3>
			<hr>
			<div class="table-responsive-sm">
			<table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>Kode distributor</th>
                    <th>Nama</th>
                    <th>No handphone</th>
                    <th>Alamat</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  if (count($dataDis) > 0) {
                  $no = 1;
                  foreach($dataDis as $ds){ ?>
					<tr>
						<td><?= $ds['kd_distributor'] ?></td>
						<td><?= $ds['nama_distributor'] ?></td>
						<td><?= $ds['no_telp'] ?></td>
						<td><?= $ds['alamat'] ?></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="?page=view_distributor&edit&id=<?= $ds['kd_distributor'] ?>" class="btn btn-info"><i class="fa fa-pencil"></i></a>
								<a href="#" class="btn btn-danger" id="btdelete<?php echo $no; ?>"><i class="fa fa-trash"></i></a>
							</div>
						</td>
					</tr>
					<script>
						$('#btdelete<?php echo $no; ?>').click(function(e){
					          e.preventDefault();
					            swal({
					            title: "Hapus",
					            text: "Yakin Hapus?",
					            type: "warning",
					            showCancelButton: true,
					            confirmButtonText: "Yes",
					            cancelButtonText: "Cancel",
					      		closeOnConfirm: false,
					      		closeOnCancel: true
					          }, function(isConfirm) {
					            if (isConfirm) {
					            	window.location.href="?page=view_distributor&delete&id=<?php echo $ds['kd_distributor'] ?>";
					            }
					          });
					        });
					</script>
                  <?php $no++; } ?>
                  <?php } ?>
                </tbody>
              </table>
          </div>
		</div>
	</div>
</div>
</div>