/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomakanan;

import java.awt.print.PrinterException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import javax.swing.JOptionPane;
import java.text.*;
import java.awt.print.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */
public class Frametransaksi extends javax.swing.JFrame {
    public Statement st;
    public ResultSet rs;
    public DefaultTableModel tabModel,tabModel1;
    Connection cn = koneksi.KoneksiDB.getKoneksi();
    

    /**
     * Creates new form Frametransaksi
     */
    public Frametransaksi() {
        initComponents();
        
        judul();
        tampil1();
        judu();
        tampil();
        tanggal();
        id();
        pack1();
        txtbayar.setEnabled(false);
        ID.setEnabled(false);
        idmenu.setEnabled(false);
        harga.setEnabled(false);
        total.setEnabled(false);
        txttotalbayar.setEnabled(false);
        txtkembali.setEnabled(false);
        
    }
    public void tanggal(){
        Date ys = new Date();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        t2.setText(s.format(ys));
        
    }
    public void judul(){
        Object[] judul = {"id","Nama Menu","Harga","Jumlah","Total"};
        tabModel = new DefaultTableModel(null,judul);
        keranjang.setModel(tabModel);
    }
     public void judu(){
        Object[] judul = {"No","id","Nama Menu","Harga","Kolom Stock"};
        tabModel1 = new DefaultTableModel(null,judul);
        keranjang1.setModel(tabModel1);
    }
      public void tampil()
    {
        try {
            st = cn.createStatement();
            tabModel1.getDataVector().removeAllElements();
        tabModel1.fireTableDataChanged();
            rs = st.executeQuery("select * from tb_menu ");
            int NO = 0;
        while (rs.next()) {
            NO++;
            Object[] data = {
                NO,
                rs.getString("id"),
                rs.getString("nama_menu"),
                rs.getString("harga"),
                rs.getString("stok"),
            };
            tabModel1.addRow(data);
        }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void cetak(){
        try {
            JTextPane jtp = new JTextPane();
            st = cn.createStatement();
            rs = st.executeQuery("select * from tb_menu, tb_transaksi where tb_menu.id = tb_transaksi.id_menu and tb_transaksi.id = '"+ID.getText()+"'");
            jtp.setContentType("text/html");
            String a = "", d = "";
            String b = "<h2 align='center'>Struk Pembelian</h2><br>"
                    + "<table>"
                    + "<tr>"
                    + "<td>JL.Alibabamakin jaya </td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td>Kode Transaksi : "+ID.getText()+" </td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td>tanggal : "+t2.getText()+"</td>"
                    + "</tr>"
                    + "</table>"
                    + "<table align='center'>"
                    +"<tr>"
                    +"<td>Nama Makanan</td>"
                    +"<td>Harga</td>"
                    +"<td>Jumlah</td>"
                    +"<td>Total</td>"
                    + "</tr>";
            while(rs.next()){
                 a +="<tr>"
                    +"<td>"+rs.getString("nama_menu")+"</td>"
                    +"<td>"+rs.getString("harga")+"</td>"
                    +"<td>"+rs.getString("jumlah")+"</td>"
                    +"<td>"+rs.getString("total")+"</td>"
                    + "</tr>";
            }
            String c = "<tr>"
                    +"<td colspan='2'></td><td>Total Bayar</td>"
                    ;
            rs = st.executeQuery("select sum(total) as total_bayar from tb_transaksi where id = '"+ID.getText()+"'");
                    while(rs.next()){
                 d = "<td>"
                    + rs.getString("total_bayar")
                    + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td colspan='2'></td><td>Bayar</td><td>"
                    + txtbayar.getText()
                    + "</td>"
                    + "</tr><tr>"
                         + "<td colspan='2'></td><td>Kembali</td><td>"
                    + txtkembali.getText()
                    + "</td></tr>"
                    + "</table>";
            }
            
            
            jtp.setText(b + a + c +d);
            jtp.print();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void tampil1()
    {
        try {
            st = cn.createStatement();
            tabModel.getDataVector().removeAllElements();
        tabModel.fireTableDataChanged();
            rs = st.executeQuery("select * from tb_menu, tb_transaksi where tb_menu.id = tb_transaksi.id_menu and tb_transaksi.id = '"+ID.getText()+"'");
        while (rs.next()) {
            Object[] data = {
                rs.getString("tb_transaksi.id"),
                rs.getString("nama_menu"),
                rs.getString("harga"),
                rs.getString("jumlah"),
                rs.getString("total")
            };
            tabModel.addRow(data);
        }
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
   
    
    public void id(){
        try {
            st = cn.createStatement();
            rs = st.executeQuery("select * from tb_transaksi");
            while (rs.next()) {                
                if (rs.first() == false) {
                    ID.setText("1");
                }
                else{
                    rs.last();
                    int auto = rs.getInt(1) + 1;
                    ID.setText(String.valueOf(auto));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void reset(){
        jumlah.setText("");
        total.setText("");
        txtbayar.setText("");
        txtkembali.setText("");
        txttotalbayar.setText("");
    }
    public void pack1(){
        btnselesai.setEnabled(false);
        btnbatal.setEnabled(false);
        btnsimpan.setEnabled(true);
        btnbayar.setEnabled(true);
    }
    public void pack2(){
        btnselesai.setEnabled(true);
        btnbatal.setEnabled(true);
        btnsimpan.setEnabled(false);
        btnbayar.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel1 = new java.awt.Panel();
        label3 = new java.awt.Label();
        label5 = new java.awt.Label();
        idmenu = new javax.swing.JTextField();
        harga = new javax.swing.JTextField();
        label2 = new java.awt.Label();
        label6 = new java.awt.Label();
        jumlah = new javax.swing.JTextField();
        total = new javax.swing.JTextField();
        btnsimpan = new javax.swing.JButton();
        btnbatal = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        keranjang = new javax.swing.JTable();
        label7 = new java.awt.Label();
        label8 = new java.awt.Label();
        label9 = new java.awt.Label();
        txtbayar = new javax.swing.JTextField();
        txttotalbayar = new javax.swing.JTextField();
        txtkembali = new javax.swing.JTextField();
        btnbayar = new javax.swing.JButton();
        btnselesai = new javax.swing.JButton();
        label10 = new java.awt.Label();
        ID = new javax.swing.JTextField();
        btnkembali = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        keranjang1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        t2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panel1.setBackground(new java.awt.Color(51, 255, 204));
        panel1.setForeground(new java.awt.Color(255, 153, 153));

        label3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label3.setForeground(new java.awt.Color(255, 255, 255));
        label3.setText("ID  Menu        :");

        label5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label5.setForeground(new java.awt.Color(255, 255, 255));
        label5.setText("Harga              :");

        label2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label2.setForeground(new java.awt.Color(255, 255, 255));
        label2.setText("Jumlah            :");

        label6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label6.setForeground(new java.awt.Color(255, 255, 255));
        label6.setText("Total                :");

        jumlah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jumlahActionPerformed(evt);
            }
        });
        jumlah.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jumlahKeyReleased(evt);
            }
        });

        btnsimpan.setText("Simpan");
        btnsimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsimpanActionPerformed(evt);
            }
        });

        btnbatal.setText("Hapus");
        btnbatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbatalActionPerformed(evt);
            }
        });

        keranjang.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        keranjang.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                keranjangMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(keranjang);

        label7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label7.setForeground(new java.awt.Color(255, 255, 255));
        label7.setText("Bayar                 :");

        label8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label8.setForeground(new java.awt.Color(255, 255, 255));
        label8.setText("Total Bayar        :");

        label9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label9.setForeground(new java.awt.Color(255, 255, 255));
        label9.setText("Kembali             :");

        txtbayar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbayarKeyReleased(evt);
            }
        });

        btnbayar.setText("Bayar");
        btnbayar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbayarActionPerformed(evt);
            }
        });

        btnselesai.setText("Selesai");
        btnselesai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnselesaiActionPerformed(evt);
            }
        });

        label10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        label10.setForeground(new java.awt.Color(255, 255, 255));
        label10.setText(" ID                   :");

        btnkembali.setText("Kembali");
        btnkembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnkembaliActionPerformed(evt);
            }
        });

        keranjang1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        keranjang1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                keranjang1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(keranjang1);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Tanggal          :");

        t2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        t2.setForeground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Daftar Menu");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Pesanan");

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(277, 277, 277)
                        .addComponent(jLabel3))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 401, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel1Layout.createSequentialGroup()
                                .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txttotalbayar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtbayar, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panel1Layout.createSequentialGroup()
                                .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(41, 41, 41)
                                .addComponent(t2)
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(42, 42, 42))))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label5, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(total, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE)
                            .addComponent(jumlah, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(harga, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(idmenu, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ID, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsimpan)
                                .addGap(87, 87, 87)
                                .addComponent(btnbatal, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(52, 52, 52))
                            .addGroup(panel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addContainerGap())))
                    .addGroup(panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(label8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGap(124, 124, 124)
                                    .addComponent(btnbayar)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(btnselesai))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(t2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ID, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(idmenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(harga, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(total, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnbatal)
                        .addComponent(btnsimpan)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttotalbayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtkembali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnbayar)
                    .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnselesai)))
                .addGap(18, 18, 18)
                .addComponent(btnkembali)
                .addGap(199, 199, 199))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, 589, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void keranjang1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_keranjang1MouseClicked
        // TODO add your handling code here:
        int row = keranjang1.getSelectedRow();
        idmenu.setText((String)keranjang1.getValueAt(row,1));
        harga.setText((String)keranjang1.getValueAt(row,3));
        tampil1();
        tampil();
    }//GEN-LAST:event_keranjang1MouseClicked

    private void btnkembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnkembaliActionPerformed
        // TODO add your handling code here:
        Framelogin fp = new Framelogin();
        fp.show();
        this.dispose();
    }//GEN-LAST:event_btnkembaliActionPerformed

    private void btnselesaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnselesaiActionPerformed
        // TODO add your handling code here:
        if (Integer.parseInt(txtbayar.getText()) >= Integer.parseInt(txttotalbayar.getText())) {
            cetak();
            id();
            tampil();
            tampil1();
            pack1();
            reset();

        }else{
            JOptionPane.showMessageDialog(null,"Uang Anda Kurang");
        }

    }//GEN-LAST:event_btnselesaiActionPerformed

    private void btnbayarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbayarActionPerformed
        // TODO add your handling code here:
        try {
            reset();
            st = cn.createStatement();
            rs = st.executeQuery("select sum(total) as total_bayar from tb_transaksi where id = '"+ID.getText()+"'");
            while(rs.next()){
                txttotalbayar.setText(rs.getString("total_bayar"));
            }
            txtbayar.setEnabled(true);
            btnselesai.setEnabled(true);
            btnsimpan.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnbayarActionPerformed

    private void txtbayarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbayarKeyReleased
        // TODO add your handling code here:
        int c = Integer.parseInt(txtbayar.getText()) - Integer.parseInt(txttotalbayar.getText());
        txtkembali.setText(String.valueOf(c));
    }//GEN-LAST:event_txtbayarKeyReleased

    private void keranjangMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_keranjangMouseClicked
        // TODO add your handling code here:
        int row = keranjang.getSelectedRow();
        ID.setText((String)keranjang.getValueAt(row,0));
        harga.setText((String)keranjang.getValueAt(row,2));
        jumlah.setText((String)keranjang.getValueAt(row,3));
        total.setText((String)keranjang.getValueAt(row,4));
        tampil();
        tampil1();
        pack2();
    }//GEN-LAST:event_keranjangMouseClicked

    private void btnbatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbatalActionPerformed
        // TODO add your handling code here:
        try {
            int stock = 0;
            st = cn.createStatement();
            st.executeUpdate("delete from tb_transaksi where id = '"+ID.getText()+"' and id_menu = '"+idmenu.getText()+"' and jumlah = '"+jumlah.getText()+"'");
            rs = st.executeQuery("select * from tb_menu where id = '"+idmenu.getText()+"' ");
            while(rs.next()){
                stock = rs.getInt(5);
            }
            int hasil = stock + Integer.parseInt(jumlah.getText());
            st.executeUpdate("update tb_menu set stok = '"+hasil+"' where id = '"+idmenu.getText()+"' ");
            tampil();
            tampil1();
            reset();
            pack1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnbatalActionPerformed

    private void btnsimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsimpanActionPerformed
        // TODO add your handling code here:
        try {
            int stock = 0;
            String cek="", harga = "";
            st = cn.createStatement();
            rs = st.executeQuery("select * from tb_menu, tb_transaksi where tb_menu.id = tb_transaksi.id_menu and tb_transaksi.id_menu = '"+idmenu.getText()+"' AND tb_transaksi.id = '"+ID.getText()+"'");
            while(rs.next()){
                cek = rs.getString("jumlah");
            }
            if(cek == ""){
                st.executeUpdate("insert into tb_transaksi set id='"+ID.getText()+"', id_menu = '"+idmenu.getText()+"', jumlah = '"+jumlah.getText()+"', total = '"+total.getText()+"', tanggal = '"+t2.getText()+"'");
                rs = st.executeQuery("select * from tb_menu where id = '"+idmenu.getText()+"' ");
                while(rs.next()){
                    stock = rs.getInt(5);
                }
                int hasil = stock - Integer.parseInt(jumlah.getText());
                st.executeUpdate("update tb_menu set stok = '"+hasil+"' where id = '"+idmenu.getText()+"' ");

            }else{
                rs = st.executeQuery("select * from tb_menu where id = '"+idmenu.getText()+"' ");
                while(rs.next()){
                    stock = rs.getInt(5);
                    harga = rs.getString("harga");
                }
                int hasil = Integer.parseInt(cek) + Integer.parseInt(jumlah.getText());
                int b = Integer.parseInt(harga);
                int c = hasil * b;
                st.executeUpdate("update tb_transaksi set jumlah = '"+hasil+"', total = '"+c+"' where id_menu = '"+idmenu.getText()+"' AND id = '"+ID.getText()+"' ");
                int ha = stock - Integer.parseInt(cek);
                st.executeUpdate("update tb_menu set stok = '"+ha+"' where id = '"+idmenu.getText()+"'");

            }
            judul();
            tampil();
            tampil1();
            reset();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnsimpanActionPerformed

    private void jumlahKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jumlahKeyReleased
        // TODO add your handling code here:
        int a = Integer.parseInt(harga.getText());
        int b = Integer.parseInt(jumlah.getText());
        int c = a * b;
        total.setText(String.valueOf(c));
    }//GEN-LAST:event_jumlahKeyReleased

    private void jumlahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jumlahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jumlahActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frametransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frametransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frametransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frametransaksi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frametransaksi().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ID;
    private javax.swing.JButton btnbatal;
    private javax.swing.JButton btnbayar;
    private javax.swing.JButton btnkembali;
    private javax.swing.JButton btnselesai;
    private javax.swing.JButton btnsimpan;
    private javax.swing.JTextField harga;
    private javax.swing.JTextField idmenu;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jumlah;
    private javax.swing.JTable keranjang;
    private javax.swing.JTable keranjang1;
    private java.awt.Label label10;
    private java.awt.Label label2;
    private java.awt.Label label3;
    private java.awt.Label label5;
    private java.awt.Label label6;
    private java.awt.Label label7;
    private java.awt.Label label8;
    private java.awt.Label label9;
    private java.awt.Panel panel1;
    private javax.swing.JLabel t2;
    private javax.swing.JTextField total;
    private javax.swing.JTextField txtbayar;
    private javax.swing.JTextField txtkembali;
    private javax.swing.JTextField txttotalbayar;
    // End of variables declaration//GEN-END:variables
}
