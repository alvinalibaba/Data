<?php
	error_reporting(0);

	$qb = new KONTROLER();

	if (isset($_POST['btnSearch'])) {
		$whereparam = "tanggal_masuk";
		$param      = $_POST['dateAwal'];
		$param1     = $_POST['dateAkhir'];
		$dataB      = $qb->selectBetween("detailbarang",$whereparam,$param,$param1);
	}
 ?>
<div class="col-sm-12">
	<div class="tile">
		<h3>Periode</h3>
		<hr>
	<form method="post">
		<div class="row">
			<div class="col-sm-3">
				<label for="#">Dari Tanggal</label>
				<input class="form-control" type="date" placeholder="Select Date" name="dateAwal" required>
			</div>
			<div class="col-sm-3">
				<label for="#">Ke Tanggal</label>
				<input class="form-control" type="date" placeholder="Select Date" name="dateAkhir" required>
			</div>
			<div class="col-sm-6">
				<div style="margin-top: 27px;">
				<button class="btn btn-secondary" name="btnSearch"><i class="fa fa-search"></i> Cari</button>
				<a href="?page=periode" class="btn btn-white"><i class="fa fa-repeat"></i>Reload</a>
				<?php if (isset($_POST['dateAwal'])): ?>
					<a href="print_barangPeriode.php?dateAwal=<?php echo $param ?>&dateAkhir=<?php echo $param1 ?>" class="btn btn-primary"><i class="fa fa-print"></i> Print</a>
					<a href="export_barangPeriode.php" name="export" class="btn btn-success" target="_blank"><i class="fa fa-file-excel-o"></i>Export</a>
				<?php endif ?>
				</div>
			</div>
		</div>
	</form>
	<hr>
	<br>
	<div class="table-responsive-sm">
	<table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Kode barang</th>
                <th>Nama barang</th>
                <th>Merek</th>
                <th>Distributor</th>
                <th>Tanggal Masuk</th>
                <th>Harga</th>
                <th>Stok</th>
            </tr>
        </thead>
        <tbody>
        <?php
        if (count(@$dataB) > 0) {
            $no = 1;
            foreach(@$dataB as $ds){ ?>
			<tr>
				<td><?= $ds['kd_barang'] ?></td>
				<td><?= $ds['nama_barang'] ?></td>
				<td><?= $ds['merek'] ?></td>
				<td><?= $ds['nama_distributor'] ?></td>
				<td><?= $ds['tanggal_masuk'] ?></td>
				<td><?= number_format($ds['harga_barang']) ?></td>
				<td><?= $ds['stok_barang'] ?></td>
        <?php $no++; } }else{ ?>
			<tr>
				<td colspan="7" class="text-center">Tidak ada barang di periode ini</td>
			</tr>
        <?php } ?>
        </tbody>
    </table>
    </div>
	</div>
</div>
