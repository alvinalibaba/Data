<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Dashboard Admin</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/mains.css">
    <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/iconfont/material-icons.css">

    <script src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
    
    <style type="text/css">
      @font-face {
        font-family: 'Orbitron';
        src: url('Orbitron/Orbitron-Regular.ttf');
        src: url('Orbitron/Orbitron-Regular.ttf') format('ttf'),
             url('Orbitron/Orbitron-Regular.ttf') format('truetype');
      }
    </style>
  </head>
  <?php 
  include "kontroler.php";
  $iv = new KONTROLER();
  session_start();
  $Auth = $iv->AuthUser($_SESSION['username']);
  if (isset($_GET['logout'])) {
    $iv->logout();
  }

  if ($iv->sessionCheck() == "false") {
    header("location:login.php");
  }

  if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  }
 ?>
  <body class="app sidebar-mini rtl">
    <header class="app-header"><a class="app-header__logo" href="#" style="font-size: 2em;">Sarpras</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item fa fa-user-circle fa-2x" href="#" data-toggle="dropdown" aria-label="Open Profile Menu" style="text-decoration: none;"></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="?page=profile"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="#" id="forLogout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu -->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user">
        <div>
          <p class="app-sidebar__user-name"><?php echo $Auth['nama_user'] ?></p>
          <p class="app-sidebar__user-designation"><?php echo $_SESSION['level']; ?></p>
        </div>
      </div>
      <hr>
      <ul class="app-menu">
        <li><a class="app-menu__item" href="?page"><i class="material-icons" style="margin-right: 5px;">dashboard</i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="?page=view_distributor"><i class="material-icons" style="margin-right: 5px;">people</i><span class="app-menu__label">Distributor</span></a></li>
        <li><a class="app-menu__item" href="?page=view_merek"><i class="material-icons" style="margin-right: 5px;">branding_watermark</i><span class="app-menu__label">Merek</span></a></li>
        <li><a class="app-menu__item" href="?page=view_barang"><i class="material-icons" style="margin-right: 5px;">widgets</i><span class="app-menu__label">Barang</span></a></li>
        </li>
      </ul>
    </aside>
    <main class="app-content">

      <?php 
        @$page = $_GET['page'];
        switch ($page) {
          case 'view_barang':
            include "view_barang.php";
            break;
          case 'addBarang':
            include "view_addBarang.php";
            break;
          case 'view_distributor':
            include "view_distributor.php";
            break;
          case 'view_merek':
            include "view_merek.php";
            break;
          case 'view_editBarang':
            include "view_editBarang.php";
          break;
          case 'view_detail':
            include "view_detailBarang.php";
          break;
          case 'profile':
            include "profile.php";
          break;
          case 'image_view':
            include "view_imagebarang.php";
          break;
          default:
            include "dashboard.php";
            break;
        }
       ?>
    </main>


    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript" src="js/plugins/chart.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
      $('#sampleTable').DataTable();
      $('#disTable').DataTable();
    </script>
    <?php include "alerts_response.php"; ?>
    <script>
      $(document).ready(function(){
        $('#forLogout').click(function(e){
          e.preventDefault();
            swal({
            title: "Logout",
            text: "Yakin Logout?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
          }, function(isConfirm) {
            if (isConfirm) {
              window.location.href="?logout";
            }
          });
        });
        $('#raditor').DataTable();
      });
    </script>
    <script type="text/javascript">
      var data = {
        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        datasets: [
          {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [0, 0, 0, 0, 0, 0, 100, 100, 0, 0, 0, 0]
          },
          {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: [10, 10, 30, 30, 45, 45, 100, 100, 50, 50, 5, 5]
          }
        ]
      };
      var enol = 0;
      var pdata = [
        {
          value: 15,
          color: "green",
          highlight: "lightseagreen",
          label: "Complete"
        },
        {
          value: 35,
          color:"red",
          highlight: "indigo",
          label: "In-Progress"
        },
        {
          value: 50,
          color: "green",
          highlight: "teal",
          label: "Complete"
        }, 
      ]
      
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);
      
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    </script>
  </body>
</html>