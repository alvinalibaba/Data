<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="shortcut icon" src="favicon.png">
  <link rel="stylesheet" type="text/css" href="css/mains.css">
  <link rel="stylesheet" type="text/css" href="font-awesome-4.7.0/css/font-awesome.min.css">

    <?php
		include "kontroler.php";
		session_start();
		$iv = new KONTROLER();
		if ($iv->sessionCheck() == "true") {
			if ($_SESSION['level'] == "Admin") {
				header("location:PageAdmin.php");
			}else if($_SESSION['level'] == "Kasir"){
				header("location:PageKasir.php");
			}else if($_SESSION['level'] == "Manager"){
				header("location:PageManager.php");
			}
		}
		if (isset($_POST['Login'])) {
			$username = $_POST['username'];
			$password = $_POST['password'];
			if ($response = $iv->login($username,$password)) {
				if ($response['response'] == "positive") {
					$_SESSION['username'] = $_POST['username'];
					$_SESSION['level']    = $response['level'];
					if ($response['level'] == "Admin") {
						echo "<script>window.location.href='PageAdmin.php'</script>";
					}else if($response['level'] == "Kasir"){
						echo "<script>window.location.href='PageKasir.php'</script>";
					}else if($response['level'] == "Manager"){
						echo "<script>window.location.href='PageManager.php'</script>";
					}
				}
			}
		}
	 ?>
    <style>
      @font-face {
        font-family: 'Orbitron';
        src: url('Orbitron/Orbitron-Regular.ttf');
        src: url('Orbitron/Orbitron-Regular.ttf') format('ttf'),
             url('Orbitron/Orbitron-Regular.ttf') format('truetype');
      }
    	.form-control::focus{
    		background-color: #172029;
    	}
    	.logo-new{
    		margin-top: -80px;
    		margin-bottom: -10px;
    		font-size: 40px;
    	}
      .samping{
        background-color: #444;
      }
      h1{
        font-size: 50px;
      }
      .judul{
        font-weight: bold;
        color: white;
      }
    </style>
</head>
<body>
<div class="container-fluid samping">
    <section class="login-content">
      <h1 class="judul text-center" style="font-family: Orbitron; font-size: 2.3rem;">Invenstory</h1>
      <h2 class="judul text-center" style="font-family: Consolas; font-size: 1.9rem;">Cerita Gudangmu</h2><br>
      <div class="login-box" style="border-radius: 5px;">
        <form class="login-form" method="post">
          <br><br>
          <div class="form-group">
            <label class="control-label" for="username">USERNAME</label>
            <input class="form-control" type="text" name="username" autocomplete="off" autofocus id="username">
          </div>
          <div class="form-group">
            <label class="control-label" for="password">PASSWORD</label>
            <input class="form-control" type="password" name="password" id="password">
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input type="checkbox" id="Stay"><span class="label-text" for="Stay">Stay Signed in</span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-secondary btn-block" name="Login"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
      </div>
    </section>
</div>
</div>



	<script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>
  <script src="js/plugins/pace.min.js"></script>
  <script type="text/javascript" src="js/plugins/sweetalert.min.js"></script>
  <?php include "alerts_response.php" ?>
</body>
</html>
