<?php 
	include "koneksi.php";
	include "controler.php";
	@session_start();
	$perintah = new oop();
	if ($_SESSION['user']==null) {
		?>
		<script>
			window.location.href="index.php";
		</script>
		<?php
	}
 ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume/CV-Muhammad Firman Prayoga</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">
    
  </head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger pull-left" style="margin-left: -60px" href="admin.php">My Resume-CV</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
    <div class="col-col-12">   			
	<?php 
	@$menu = $_GET['q'];
	
	if ($menu=="header") {
		if (isset($_POST['done'])) {
			$query = mysqli_query($con,"truncate table header");
			$head =$_POST['head'];
			$desk =$_POST['desk'];
			$perintah->simpan($con,"header",array("header"=>$head,"deskripsi"=>$desk));
			?>
			<script>
				alert("seuccess");
				window.location.href="admin.php";
			</script>
			<?php	
		}
		?>
		<header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
          <div class="col-md-10 mx-auto">
        		
        	<center>
        	<h2 style="color:coral; margin-bottom: 50px;">Edit Header</h2>
        		
            <div class="col-md-8">
        	<form method="POST">
            	<div class="form-group">
            		<p>Header </p>
            		<input type="text" name="head" placeholder="Insert Header-Text" value="<?php 
            			$data = $perintah->tampil($con,"header");
            			$data = mysqli_fetch_array($data);
            			echo $data['header'];
            			 ?>" class="form-control">
            		<p>Description</p>
            		<input type="text" name="desk" placeholder="Insert Description-Text" value="<?php 
            			$data = $perintah->tampil($con,"header");
            			$data = mysqli_fetch_array($data);
            			echo $data['deskripsi'];
            			 ?>" class="form-control">
            	</div>
            	<button class="btn btn-primary pull-right" name="done">Done!</button>
        	</form>
            </div>
        	</center>
          </div>
      </div>
    </header>
		<?php
	}elseif ($menu=="about") {
		if (isset($_POST['done'])) {
			@$fullname = $_POST['full'];
			@$deskripsi = $_POST['desc'];
			$foto = $_FILES['foto']['name'];
			$tmp = $_FILES['foto']['tmp_name'];
			$fotobaru = date('dmYHis').$foto;
			$path = "assets/img/".$fotobaru;

			if(move_uploaded_file($tmp, $path)){
				$ava = $_FILES['ava']['name'];
				$tmp2 = $_FILES['ava']['tmp_name'];
				$fotobaru2 = date('dmYHis').$ava;
				$path2 = "assets/img/".$fotobaru2;
				if (move_uploaded_file($tmp2, $path2)) {
				?>
				<script>alert("success");window.location.href="admin.php"</script>
				<?php
				mysqli_query($con,"truncate table about");
				$isi = array('bg' => $fotobaru,'name'=>$fullname,'desk'=>$deskripsi,'ava'=>$fotobaru2);
				$sql = $perintah->simpan($con,"about",$isi);
			}else{
				echo " ".$path2;
			}
			}else{
				echo "hhj";

			}
		}
		$query = mysqli_query($con,"select * from about");
         while ($data=mysqli_fetch_assoc($query)) {
		?>
<header class="masthead text-center text-white d-flex">
      <div class="col-col-12" >
        
        <div class="col-col-12" align="center" style="margin-top: 80px">
        	<center>
        	<h2 style="color:coral; margin-bottom: -10px;">Edit About</h2>
			<hr>
			<form method="POST" enctype="multipart/form-data">
				<div class="col-md-12" align="center">
				<div class="form-group">
					<p class="pull-left">Choose Background Image</p>
					<input type="file" name="foto" class="form-control" required="">
				</div>
				<div class="form-group">
					<p class="pull-left">Choose Avatar</p>
					<input type="file" name="ava" class="form-control" required="">
				</div>
				<div class="form-group">
					<p class="pull-left">Fullname</p>
					<input type="text" name="full" class="form-control" placeholder="Fullname" required="">
				</div>
				<div class="form-group">
					<p class="pull-left">Description</p>
					<input type="text" name="desc" class="form-control" placeholder="Description" required="">
				</div>
			<button class="btn btn-primary pull-right" name="done" style="margin-bottom: 10px">Done!</button>

			</div>
			</form>        	
        	</center>
        </div>
      </div>
     <div class="col-col-2"> 
     <div style="margin-top: 40px;width:722px;height: 600px; background: url(assets/img/<?php echo $data['bg'] ?>) ; background-size: cover;" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">
            <h2 class="section-heading text-white" style="margin-top: 20px;"><?php echo $data['name'] ?></h2>
            <hr>
            <img src="assets/img/<?php echo $data['ava'] ?>" class="img-circle" width="200" height="200" style="border-radius: 100%">
            <hr >
            <p class="text-faded mb-4"><?php echo $data['desk'] ?></p>
            
          </div>
        </div>
      </div>
      </div>
  </div>
</header>
		<?php		
		}
	}elseif ($menu=="skill") {
		@$id = $_GET['id'];
		@$aksi = $_GET['adksi'];
		if ($aksi==null) {
		if (isset($_POST['done'])) {
			$title =$_POST['tittle'];
			$skill = $_POST['skill'];
			$tmp = $_FILES['icon']['tmp_name'];
			$foto = $_FILES['icon']['name'];
			$tmp = $_FILES['icon']['tmp_name'];
			$fotobaru = date('dmYHis').$foto;
			$path = "assets/img/".$fotobaru;

			if(move_uploaded_file($tmp, $path)){
				$perintah->simpan($con,"skill",array('tittle'=>$title,'image'=>$fotobaru,'skill'=>$skill));
				?>
				<script>
					alert("success");
					window.location.href="admin.php";
				</script>
				<?php
			}
		}
		?>
		<header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
          <div class="col-md-10 mx-auto">
        		
        	<center>
        	<h2 style="color:coral; margin-bottom: 50px;">New Skill</h2>
            <div class="col-md-8">
        	<form method="POST" enctype="multipart/form-data">
            	<div class="form-group">
            		<p>Tittle </p>
            		<input type="text" name="tittle" placeholder="Insert Tittle" class="form-control" required="">
            		<p>Icon Skill</p>
            		<input type="file" name="icon" class="form-control" required="">
            			 <p>Skill in Persent </p>
            		<input type="number" name="skill" placeholder="Insert Skill"  class="form-control" required="">
            	</div>
            	<button class="btn btn-primary pull-right" name="done">Done!</button>
        	</form>
            </div>
        	</center>
          </div>
      </div>
    </header>
    <?php
		}elseif ($aksi=="edit") {
			if (isset($_POST['update'])) {
				$title = $_POST['title'];
				$skil= $_POST['skill'];
				$tmp = $_FILES['foto']['tmp_name'];
				$foto = $_FILES['foto']['name'];
				$tmp = $_FILES['foto']['tmp_name'];
				$fotobaru = date('dmYHis').$foto;
				$path = "assets/img/".$fotobaru;

				if(move_uploaded_file($tmp, $path)){
					$perintah->ubah($con,"skill",array('image'=>$fotobaru,'tittle'=>$title,'skill'=>$skil),"id='$id'","admin.php");
				}
				}
			?>
			<header class="masthead text-center text-white d-flex">
      <div class="col-col-12" >
        
        <div class="col-col-12" align="center" style="margin-top: 80px">
        	<center>
        	<h2 style="color:coral; margin-bottom: -10px;">Edit Skill</h2>
			<hr>
			<form method="POST" enctype="multipart/form-data">
				<div class="col-md-12" align="center">
				<div class="form-group">
					<p class="pull-left">Choose Skill Icon</p>
					<input type="file" name="foto" class="form-control" required="">
				</div>
				<div class="form-group">
					<p class="pull-left">Title</p>
					<input type="text" name="title" class="form-control" placeholder="Insert Title" required="">
				</div>
				<div class="form-group">
					<p class="pull-left">Capability</p>
					<input type="number" name="skill" class="form-control" placeholder="Capability" required="">
				</div>
			<button class="btn btn-primary pull-right" name="update" style="margin-bottom: 10px">Update!</button>

			</div>
			</form>        	
        	</center>
        </div>
      </div>
    <div class="col-col-2"> 
    	 <?php 
    	 if (isset($_POST['delete'])) {
    	 	$perintah->hapus($con,"skill","id=$id","admin.php");
    	 }
    	 $data = $perintah->tampilwhere($con,"skill","id=$id");
    	 while ($data = mysqli_fetch_array($data)) {
?>			
			<div class="col-col-12" style="margin-top: 100px;padding: 10px; border-radius: 10px; margin-left: 200px; background-color: white; ">
          	<h2 style="color: coral"><?php echo $data['tittle'] ?></h2>
          	<a href="edit.php?q=skill&adksi=edit&id=<?php echo $data['id'] ?>"><img src="assets/img/<?php echo $data['image'] ?>" width="100px"  height="100px"></a>
          	<div style="width: 250px; background-color: coral; margin-top: 20px; border-radius: 7px; color: white">
          		<?php echo $data['skill'] ?>%
          	</div>
			</div>
			<form method="POST">
			<button class="btn btn-primary" name="delete" style="margin-top: 10px; margin-left: 200px;">Delete</button>
			</form>
  	</div>
</header>
<?php
    	 }      	
		}
	}
	 ?>
   		
	</div>
</body>
</html>