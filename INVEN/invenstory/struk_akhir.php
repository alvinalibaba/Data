<?php 
	$struk  = new KONTROLER();
	$id = $_GET['id'];
	$data   = $struk->edit("transaksi","kd_transaksi",$id);
	$total  = $struk->selectSumWhere("transaksi","sub_total","kd_transaksi='$id'");
 ?>
 <style>
 	.col-sm-8{
 		background: white;
 		padding: 20px;
 	}
 	@media print{
 		table{
 			align-content: center;
 		}
 		.ds{
 			display: none;
 		}
 		.tile{
 			box-shadow: none;
 		}
 		hr{
 			display: none;
 		}
 	}
 </style>
<div class="row">
	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<!-- <div class="tile"> -->
			<h3>Struk Pembelian</h3>
			<br>
			<br>
			<hr>
			<table class="table table-bordered table-striped">
				<tr>
					<th>Kode Antrian</th>
					<th>Nama Barang</th>
					<th>Jumlah Beli</th>
					<th>Sub Total</th>
				</tr>
				<?php foreach($data as $dst){ ?>
				<tr>
					<td><?= $dst['kd_pretransaksi'] ?></td>
					<td><?= $dst['nama_barang'] ?></td>
					<td><?= $dst['jumlah'] ?></td>
					<td><?= $dst['sub_total'] ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="3" class="text-left">Total</td>
					<td><?php echo $total['sum']; ?></td>
				</tr>
			</table>
			<a href="#" class="btn btn-info ds" onclick="window.print()"><i class="fa fa-print"></i> Cetak Struk</a>
			<a href="?" class="btn btn-danger ds">Kembali</a>
		<!-- </div> -->
</div>