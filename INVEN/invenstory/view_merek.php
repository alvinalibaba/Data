<?php 
	$me = new KONTROLER();
	if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  	}
	$table    = "table_merek";
	$dataDis  = $me->select($table);
	$autokode = $me->autokode($table,"kd_merek","ME");

	if (isset($_GET['delete'])) {
		$id       = $_GET['id'];
		$cek      = $me->selectCountWhere("table_barang","kd_barang","kd_merek='$id'");
		// echo $cek['count'];
		if ($cek['count'] > 0) {
			$response = ['response'=>'negative','alert'=>'Merek ini sudah di pakai di barang tidak dapat di hapus'];
		}else{
		$where    = "kd_merek";
		$response = $me->delete($table,$where,$id,"?page=view_merek");
		}
	}

	if (isset($_POST['getSave'])) {
		$kode_merek = $me->validateHtml($_POST['kode_merek']);
		$merek      = $me->validateHtml($_POST['merek']);

		if ($kode_merek == "" || $merek == "") {
			$response = ['response'=>'negative','alert'=>'lengkapi field'];
		}else{
			$value    = "'$kode_merek','$merek'";
			$response = $me->insert($table,$value,"?page=view_merek");
		}
	}

	if (isset($_POST['getUpdate'])) {
		$kode_merek = $me->validateHtml($_POST['kode_merek']);
		$merek      = $me->validateHtml($_POST['merek']);

		if ($kode_merek == "" || $merek == "") {
			$response = ['response'=>'negative','alert'=>'lengkapi field'];
		}else{
			$value    = "kd_merek='$kode_merek',merek='$merek'";
			$response = $me->update($table,$value,"kd_merek",$_GET['id'],"?page=view_merek");
		}
	}

	if (isset($_GET['edit'])) {
		$editData = $me->selectWhere($table,"kd_merek",$_GET['id']);
	}
	
 ?>
<div class="row">
	<div class="col-md-6 col-sm-12">
		<div class="tile">
			<h3>Tambah Merek</h3>
			<hr>
			<form method="post">
			<div class="form-group">
				<label for="">Kode Merek</label>
				<input type="text" class="form-control form-control-sm" name="kode_merek" value="<?php echo $autokode; ?>" readonly>
			</div>
			<div class="form-group">
				<label for="">Nama Merek</label>
				<input type="text" class="form-control form-control-sm" name="merek" value="<?php echo @$editData['merek'] ?>">
			</div>
			<?php if (isset($_GET['edit'])): ?>
			<button type="submit" name="getUpdate" class="btn btn-warning"><i class="fa fa-check"></i> Update</button>
			<a href="?page=view_merek" class="btn btn-danger">Cancel</a>
			<?php endif ?>
			<?php if (!isset($_GET['edit'])): ?>	
			<button type="submit" name="getSave" class="btn btn-primary"><i class="material-icons">save</i><center> Simpan</center></button>
			<?php endif ?>
		</div>
		</form>
	</div>
	<div class="col-md-6 col-sm-12">
		<div class="tile">
			<h3>Data Merek</h3>
			<hr>
			<table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>Kode Merek</th>
                    <th>Nama</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  if (count($dataDis) > 0) {
                  	$no = 1;
                  foreach($dataDis as $ds){ ?>
					<tr>
						<td><?= $ds['kd_merek'] ?></td>
						<td><?= $ds['merek'] ?></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="?page=view_merek&edit&id=<?= $ds['kd_merek'] ?>" class="btn btn-info"><i class="fa fa-pencil" style="margin: 5px auto;"></i></a>
								<a href="#" id="btdelete<?php echo $no ?>" class="btn btn-danger"><i class="fa fa-trash" style="margin: 5px auto;color: white"></i></a>
							</div>
						</td>
					</tr>
					<script>
						$('#btdelete<?php echo $no; ?>').click(function(e){
					          e.preventDefault();
					          swal({
					            title: "Hapus",
					            text: "Yakin Hapus?",
					            type: "warning",
					            showCancelButton: true,
					            confirmButtonText: "Yes",
					            cancelButtonText: "Cancel",
					      		closeOnConfirm: false,
					      		closeOnCancel: true
					          }, function(isConfirm) {
					            if (isConfirm) {
					            	window.location.href="?page=view_merek&delete&id=<?php echo $ds['kd_merek']; ?>";
					            }
					          });
					        });
					</script>
                  <?php $no++; } ?>
                  <?php } ?>
                </tbody>
              </table>
		</div>
	</div>
</div>