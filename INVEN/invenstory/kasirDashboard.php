<?php 
	$dk = new KONTROLER();
	$berhasil = $dk->selectCount("table_transaksi","kd_transaksi");
	$now      = date("Y-m-d");
	$where    = "tanggal_beli = '$now'";
	$assoc    = $dk->selectSumWhere("table_transaksi","total_harga",$where);
	$assoc1   = $dk->selectSumWhere("table_transaksi","jumlah_beli",$where);

	// Select Limit

	$sql = "SELECT * FROM transaksi_terbaru ORDER BY kd_transaksi DESC LIMIT 0,10";
	$exe = mysqli_query($con,$sql);
 ?>
<div class="row">
	<div class="col-sm-4">
		<div class="tile">
			<h4>Mulai Transaksi</h4>
			<a href="?page=kasirTransaksi" class="btn btn-primary"><i class="material-icons">arrow_back</i><span style="font-size: 30px;"> Disini</span></a>
		</div>
		<div class="widget-small primary coloured-icon"><i class="icon fa fa-check fa-2x"></i>
            <div class="info">
              <h4>Transaksi Berhasil</h4>
              <p><b><?php echo $berhasil['count'] ?></b></p>
            </div>
        </div>

        <div class="widget-small info coloured-icon"><i class="icon fa fa-money fa-2x"></i>
            <div class="info">
              <h4>Pendapatan Hari ini</h4>
              <p><b><?php echo "Rp.".number_format($assoc['sum'])."-,"; ?></b></p>
            </div>
        </div>

        <div class="widget-small warning coloured-icon"><i class="icon fa fa-cart-plus fa-2x"></i>
            <div class="info">
              <h4>BARANG TERJUAL</h4>
              <p><b><?php echo $assoc1['sum']; ?></b></p>
            </div>
        </div>
	</div>
	<div class="col-sm-8">
          <div class="tile">
            <h3 class="tile-title">Penjualan Mingguan</h3>
            <div class="embed-responsive embed-responsive-16by9">
              <canvas class="embed-responsive-item" id="lineChartDemo"></canvas>
            </div>
          </div>
		<div class="tile">
			<h3>Transaksi Terbaru</h3>
			<table class="table table-hover table-bordered" id="sampleTable">
				<thead>
					<th>Kode Transaksi</th>
					<th>Kasir</th>
					<th>Jumlah Beli</th>
					<th>Total Harga</th>
					<th>Tanggal Beli</th>
				</thead>
				<tbody>
					<?php while($data = mysqli_fetch_assoc($exe)){ ?>
						<tr>
							<td><?php echo $data['kd_transaksi']; ?></td>
							<td><?php echo $data['nama_user']; ?></td>
							<td><?php echo $data['jumlah_beli']; ?></td>
							<td><?php echo $data['total_harga']; ?></td>
							<td><?php echo $data['tanggal_beli']; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>