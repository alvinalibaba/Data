<?php 
  include "koneksi.php";
  include "controler.php";

  @$user = $_POST['user'];
  @$pass = $_POST['pass'];
  @session_start();

  $perintah = new oop();
  if (isset($_POST['login'])) 
  {
    $perintah->login($con,"login",$user,$pass,"admin.php");
    $_session['user'] =="firman";
    }
 ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Resume/CV-Muhammad Firman Prayoga</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    
    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

  </head>

  <body id="page-top">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="index.php">My Resume-CV</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>
    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row" class="form-signin">
          <div class="col-lg-10 mx-auto">
            <h2 class="text-uppercase">
              <strong>My Resume-CV</strong>
            </h2>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
          <form autocomplete="off" method="POST">
            <div class="form-group">
              <input type="text" class="form-control" autocomplete="off" name="user" placeholder="Username" required="">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" autocomplete="off" name="pass" placeholder="password" required="">
            </div>
              
              <button class="btn btn-primary" name="login">Login</button>
          </form>
            </div>
          </div>
      </div>
    </header>
     <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>
