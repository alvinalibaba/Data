<?php 
	$dt = new KONTROLER();
	if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  	}
	$detail = $dt->select("detailBarang");
 ?>
<div class="row">
	<div class="col-sm-3">
		<div class="card">
			<div class="card-body">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Cari..">
				</div>
				<hr>
				<div class="form-group">
					<label for="">Cari berdasarkan merek</label>
					<select name="" id="" class="form-control">
						<option value="">Lihat</option>
					</select>
				</div>
				<button name="search" class="btn btn-primary btn-sm btn-block"><i class="fa fa-search"></i> Cari</button>
				<hr>
				<div class="form-group">
					<label for="">Cari berdasarkan Distributor</label>
					<select name="" id="" class="form-control">
						<option value="">Lihat</option>
					</select>
				</div>
				<button name="search" class="btn btn-primary btn-sm btn-block"><i class="fa fa-search"></i> Cari</button>
			</div>
		</div>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<?php foreach($detail as $dts){ ?>
			<div class="col-sm-4">
				<div class="bs-component">
		            <a href="?page=view_detail&id=<?php echo $dts['kd_barang'] ?>"><div class="card">
		                <div class="card-body">
		                  <h5 class="card-title"><?php echo $dts['nama_barang']; ?></h5>
		                  <h6 class="card-subtitle text-muted"><?php echo $dts['merek'] ?></h6>
		                </div><img style="min-height: 200px; width: 100%; display: block;" src="img/<?php echo $dts['gambar'] ?>" alt="Card image">
		            </div></a>
		            <br>
        		</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>