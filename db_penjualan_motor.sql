-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Feb 2018 pada 03.31
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penjualan_motor`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `motor`
--

CREATE TABLE `motor` (
  `kode_motor` varchar(4) NOT NULL,
  `nama_motor` varchar(50) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `warna` varchar(50) NOT NULL,
  `harga` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `motor`
--

INSERT INTO `motor` (`kode_motor`, `nama_motor`, `merk`, `warna`, `harga`) VALUES
('B001', 'Revoooo', 'Hondaaa', 'Hitam', 12000000),
('B002', 'Mio Soul', 'Yamaha', 'Merah Marun', 15000000),
('B003', 'Tiger', 'Honda', 'Hitam', 20000000),
('B004', 'Vario', 'Honda', 'Hitam', 10000000),
('B005', 'Vixion', 'Honda', 'Hitam', 12000000),
('B006', 'CBR', 'Honda', 'Hitam', 10000000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_login_admin`
--

CREATE TABLE `tb_login_admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_login_admin`
--

INSERT INTO `tb_login_admin` (`username`, `password`) VALUES
('aditya', 'aditya'),
('admin', '123456');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_login_pengunjung`
--

CREATE TABLE `tb_login_pengunjung` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_login_pengunjung`
--

INSERT INTO `tb_login_pengunjung` (`username`, `password`) VALUES
('aditrn8', '123456'),
('aditya', 'sukma');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(3) NOT NULL,
  `kode_motor` varchar(4) NOT NULL,
  `nama_motor` varchar(50) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `warna` varchar(50) NOT NULL,
  `harga` int(20) NOT NULL,
  `jumlahbeli` int(10) NOT NULL,
  `diskon` int(20) NOT NULL,
  `hargatotal` int(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `inputuang` int(20) NOT NULL,
  `kembalian` int(50) NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  `waktu` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `kode_motor`, `nama_motor`, `merk`, `warna`, `harga`, `jumlahbeli`, `diskon`, `hargatotal`, `nama`, `inputuang`, `kembalian`, `tanggal`, `waktu`) VALUES
(1, 'B001', 'Revoooo', 'Hondaaa', 'Hitam', 12000000, 1, 500000, 11500000, 'asep', 12000000, 500000, '20/02/2018', '09:22:08'),
(2, 'B006', 'CBR', 'Honda', 'Hitam', 10000000, 1, 500000, 9500000, 'afin', 1000000000, 990500000, '20/02/2018', '10:58:13'),
(3, 'B001', 'Revoooo', 'Hondaaa', 'Hitam', 12000000, 1, 500000, 11500000, 'adit', 50000000, 38500000, '20/02/2018', '13:37:54'),
(4, 'B003', 'Tiger', 'Honda', 'Hitam', 20000000, 1, 500000, 19500000, 'Ikiww', 50000000, 30500000, '27/02/2018', '08:18:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor`
--
ALTER TABLE `motor`
  ADD PRIMARY KEY (`kode_motor`);

--
-- Indexes for table `tb_login_admin`
--
ALTER TABLE `tb_login_admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `tb_login_pengunjung`
--
ALTER TABLE `tb_login_pengunjung`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
