<?php

  include "koneksi.php";
  include "controler.php";

  $perintah = new oop();

 ?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Creative - Start Bootstrap Theme</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/creative.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">My Resume-CV</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
           <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#Education">Education</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#skill">Skill</a>
            </li>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="login.php">Update</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
              <?php 
              error_reporting(0);
              $data = $perintah->tampil($con,"header");
              while ($data=mysqli_fetch_assoc($data)) {
              ?>
            <h1 class="text-uppercase">
              <?php echo $data['header'] ?>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5"><?php echo $data['deskripsi'] ?></p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Find Out More</a>
          </div>
        </div>
      </div>
    </header>
        <?php
              }
  $data = mysqli_query($con,"select * from about");
    while ($data=mysqli_fetch_assoc($data)) {
?>      
    <section class="bg-primary" style="background: url(assets/img/<?php echo $data['bg'] ?>) fixed; position: " id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white"><?php echo $data['name'] ?></h2>
            <hr>
            <img src="assets/img/<?php echo $data['ava'] ?>" class="img-circle" width="200" height="200" style="border-radius: 100%">
            <hr >
            <p class="text-faded mb-4"><?php echo $data['desk'] ?></p>
          </div>
        </div>
      </div>
    </section>
<?php
    }
     ?>

   <section id="Education">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">My Education</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <img src="assets/img/Graduation Cap_96px.PNG">
              <h3 class="mb-3">SD Negeri Sukamahi 01</h3>
              <p class="text-muted mb-0">Jalan Raya Cikopo selatan Kec Megamendung, Kab Bogor.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <img src="assets/img/School_96px.PNG">
              <h3 class="mb-3">SMP Negeri Ciawi 01</h3>
              <p class="text-muted mb-0">Alamat sekolah menengah pertama yang ini saya lupa</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <img src="assets/img/Student Male_100px.PNG">
              <h3 class="mb-3">SMK WIKRAMA KOTA BOGOR</h3>
              <p class="text-muted mb-0">Jalan Raya Sindang sari selanjutnya lupa</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
            <img src="assets/img/University_104px.png">
              <h3 class="mb-3">University</h3>
              <p class="text-muted mb-0">Comming soon</p>
            </div>
          </div>
        </div>
      </div>
    </section>

     <section class="p-0" id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters popup-gallery">
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="assets/img/baruu.png">
              <img class="img-fluid" src="assets/img/baruu.png" alt="" style="width: 500px;height: 250px;">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Website
                  </div>
                  <div class="project-name">
          Perpoos.com
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="assets/img/jk.png">
              <img class="img-fluid" src="assets/img/jk.png" alt="" style="width: 500px;height: 250px;">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Website
                  </div>
                  <div class="project-name">
          Perpoos.com
                  </div>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-sm-6">
            <a class="portfolio-box" href="assets/img/my.png">
              <img class="img-fluid" src="assets/img/my.png" alt="" style="width: 500px;height: 250px;">
              <div class="portfolio-box-caption">
                <div class="portfolio-box-caption-content">
                  <div class="project-category text-faded">
                    Desktop
                  </div>
                  <div class="project-name">
                    Myworkplace-Repair and Shop
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>

   <section id="skill">
      <div class="container" style="height: 450px">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">My Skill</h2>
            <hr class="my-4">
            <p class="mb-5">I'am The Coding Enthusiast, and it's my skill :</p>
          </div>
        </div>
        <div class="row" >
        <?php 
        $data = $perintah->tampil($con,"skill");
        foreach ($data as $key) {
        ?>
          <div class="col-lg-3 ml-auto text-center" style="margin-bottom: 30px">
            <h2><?php echo $key['tittle'] ?></h2>
            <a href="edit.php?q=skill&adksi=edit&id=<?php echo $key['id'] ?>"><img src="assets/img/<?php echo $key['image']?>" width="100px"  height="100px"></a>
            <div style="width: 250px; background-color: coral; margin-top: 20px; border-radius: 7px; color: white">
              <?php echo $key['skill'] ?>%
            </div>
          </div>
        <?php
        }
         ?>
        </div>
      </div>
    </section>
<section id="contact" class="bg-primary">
      <div class="container" style="color: white">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Let's Get In Touch!</h2>
            <hr class="my-4">
            <p class="mb-5">Ready to start your next project with me? That's great! Give me a call or send me an email and we will get back to you as soon as possible!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
            <p>0838-1121-2847</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
            <p>
              <a href="mailto:your-email@your-domain.com" style="color: white">Prayogafirman22@gmmail.com</a>
            </p>
          </div>
        </div>
      </div>
    </section>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/creative.min.js"></script>

  </body>

</html>
