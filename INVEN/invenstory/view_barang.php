<?php 
	$qb = new KONTROLER();
	$dataB = $qb->select("detailbarang");
	if ($_SESSION['level'] != "Admin") {
    header("location:login.php");
  	}
	if (isset($_GET['delete'])) {
		$response = $qb->delete("table_barang","kd_barang",$_GET['id'],"?page=view_barang");
	}
 ?>
<div class="row">
	<div class="col-md-12">
		<div class="tile">
			<a href="?page=addBarang" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Barang</a>
			<hr>
			<div class="table-responsive-sm">
			<table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>Kode barang</th>
                    <th>Nama barang</th>
                    <th>Merek</th>
                    <th>Distributor</th>
                    <th>Tanggal Masuk</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (count($dataB) > 0) { 
                  $no = 1;
                  foreach($dataB as $ds){ ?>
					<tr>
						<td><?= $ds['kd_barang'] ?></td>
						<td><?= $ds['nama_barang'] ?></td>
						<td><?= $ds['merek'] ?></td>
						<td><?= $ds['nama_distributor'] ?></td>
						<td><?= $ds['tanggal_masuk'] ?></td>
						<td><?= number_format($ds['harga_barang']) ?></td>
						<td><?= $ds['stok_barang'] ?></td>
						<td class="text-center">
							<div class="btn-group">
								<a href="?page=view_detail&id=<?php echo $ds['kd_barang'] ?>" class="btn btn-warning"><i class="fa fa-search" style="margin: 5px auto;"></i></a>
								<a href="?page=view_editBarang&edit&id=<?= $ds['kd_barang'] ?>" class="btn btn-info"><i class="fa fa-pencil" style="margin: 5px auto;"></i></a>
								<button id="btdelete<?php echo $no; ?>" class="btn btn-danger">
									<i class="fa fa-trash" style="margin: 5px auto;"></i>
								</button>
							</div>
						</td>
					</tr>
					<script>
						$('#btdelete<?php echo $no; ?>').click(function(e){
					          e.preventDefault();
					            swal({
					            title: "Hapus",
					            text: "Yakin Hapus?",
					            type: "warning",
					            showCancelButton: true,
					            confirmButtonText: "Yes",
					            cancelButtonText: "Cancel",
					      		closeOnConfirm: false,
					      		closeOnCancel: true
					          }, function(isConfirm) {
					            if (isConfirm) {
					            	window.location.href="?page=view_barang&delete&id=<?php echo $ds['kd_barang'] ?>";
					            }
					          });
					        });
					</script>
                  <?php $no++; } ?>
                  <?php } ?>
                </tbody>
              </table>
          </div>
		</div>
	</div>
</div>