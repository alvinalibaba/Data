<?php
include "koneksi.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Laporan</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Menu Pembimbing</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Rayonku</a></li>
      <li><a href="#">Export</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Log Out</a></li>
    </ul>
  </div>
</nav>

<div class="container">
  <h2>Daftar Rayon Suk 1</h2>
  <p></p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Nama Lengkap</th>
        <th>NIS</th>
        <th>Rombel</th>
        <th>Rayon</th>
        <th>Kehadiran</th>
      </tr>
    </thead>
    <tbody>
      <?php
      	$sql="SELECT * FROM  tbl_siswa";
      	$query=mysqli_query($conn,$sql);
      	while($data=mysqli_fetch_array($query))
      	{
      ?>
      <tr>
      	<td><?php echo $data['nama'] ?></td>
      	<td><?php echo $data['nis'] ?></td>
      	<td><?php echo $data['rombel'] ?></td>
      	<td><?php echo $data['rayon'] ?></td>
      	<td><?php echo $data['kehadiran'] ?></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>

</body>
</html>