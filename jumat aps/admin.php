<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
	<link rel="shortcut icon" href="img/islam.png">
	<link rel="stylesheet" href="css/gaya.css">
</head>
<body>
	<nav class="main-nav">
		<div class="container">
			<img src="img/islam.png" class="logo" alt="">
			<ul>
				<li><a href="contact.html">Pendamping Sholat jumat</a></li>
				<li><a href="about.html">Anggota DPR</a></li>
				<li><a href="index.html">Beranda</a></li>
			</ul>
		</div>
	</nav>
	<div class="anggota-banner">
		<div class="col-12">
			<p class="secondary-main">Sholat Jumat</p>
			<p class="secondary-second">رَبِّ أَوْزِعْنِي أَنْ أَشْكُرَ نِعْمَتَكَ الَّتِي أَنْعَمْتَ عَلَيَّ وَعَلَى وَالِدَيَّ وَأَنْ أَعْمَلَ صَالِحًا تَرْضَاهُ وَأَدْخِلْنِي بِرَحْمَتِكَ فِي عِبَادِكَ الصَّالِحِي</p>
		</div>
	</div>
	
	<footer class="main-footer">
		<p class="footer-copyright"><br>Powered By <a href="http://daffafirdaus.com" class="orange" style="text-decoration:none;">Alvin Reyvaldo</a></p>
	</footer>
	<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
	<script type="text/javascript">
		$(function() {
	$(window).scroll(function() {
		if ($(window).scrollTop() >= 50) {
			$('.main-nav').addClass('navbar-fixed');
			$('.navbar-fixed').removeClass('main-nav');
		}
		else {
			$('.navbar-fixed').addClass('main-nav');
			$('.main-nav').removeClass('navbar-fixed');
		}
	});
});

	</script>
</body>
</html>